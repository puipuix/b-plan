using BPlan.Game;
using BPlan.Game.Common;
using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.Menu;
using BPlan.Game.NetWork;
using BPlan.Game.Tests;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using Array = Godot.Collections.Array;

public class MyStartMenu : CanvasLayer
{
	private static readonly PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Menu/MyStartMenu.tscn");

	public static MyStartMenu Instance(string message)
	{
		var s = (MyStartMenu)_packedScene.Instance();
		s._message = message;
		return s;
	}

	private static bool _isInitialized = false;
	private string _message = "MSG_START";
	
	#region Play Nodes
	[GDEReady("Port", EGDEResearchMode.Descendant)]
	private LineEdit _port = null;

	[GDEReady("Ip", EGDEResearchMode.Descendant)]
	private LineEdit _ip = null;

	[GDEReady("Name", EGDEResearchMode.Descendant)]
	private LineEdit _playerName = null;

	[GDEReady("StartServer", EGDEResearchMode.Descendant)]
	private Button _startServer = null;

	[GDEReady("Connect", EGDEResearchMode.Descendant)]
	private Button _connectToServer = null;

	[GDEReady("Exit", EGDEResearchMode.Descendant)]
	private Button _exit = null;

	[GDEReady("AllowConnections", EGDEResearchMode.Descendant)]
	private CheckButton _allowConnections = null;

	#endregion

	#region Hangar Nodes

	[GDEReady("PlaneSelection", EGDEResearchMode.Descendant)]
	private OptionButton _planeSelection = null;

	[GDEReady("MENU_COLOR", EGDEResearchMode.Descendant)]
	private ColorPicker _planeColor = null;

	[GDEReady("PlanePreview", EGDEResearchMode.Descendant)]
	private TextureRect _planePreview = null;
	private ShaderMaterial _planePreviewShader;

	[GDEReady("PrimaryAmmo", EGDEResearchMode.Descendant)]
	private OptionButton _primaryAmmo = null;

	[GDEReady("SecondaryAmmo", EGDEResearchMode.Descendant)]
	private OptionButton _secondaryAmmo = null;

	[GDEReady("ThirdAmmo", EGDEResearchMode.Descendant)]
	private OptionButton _thirdAmmo = null;

	[GDEReady("FourthAmmo", EGDEResearchMode.Descendant)]
	private OptionButton _fourthAmmo = null;

	private OptionButton[] _ammoButtons;

	#endregion

	#region Volume Nodes

	[GDEReady("MasterVolume", EGDEResearchMode.Descendant)]
	private HSlider _masterVolume = null;

	[GDEReady("EnginesVolume", EGDEResearchMode.Descendant)]
	private HSlider _enginesVolume = null;

	[GDEReady("GunShotsVolume", EGDEResearchMode.Descendant)]
	private HSlider _gunShotsVolume = null;

	[GDEReady("ExplosionsVolume", EGDEResearchMode.Descendant)]
	private HSlider _explosionsVolume = null;

	[GDEReady("EnvironmentVolume", EGDEResearchMode.Descendant)]
	private HSlider _environmentVolume = null;

	[GDEReady("MusicsVolume", EGDEResearchMode.Descendant)]
	private HSlider _musicsVolume = null;

	[GDEReady("CenterContainer/GunShotDemo", EGDEResearchMode.Path)]
	private AudioStreamPlayer _gunDemo = null;

	[GDEReady("CenterContainer/ExplosionDemo", EGDEResearchMode.Path)]
	private AudioStreamPlayer _explosionDemo = null;

	[GDEReady("CenterContainer/EngineDemoStart", EGDEResearchMode.Path)]
	private AudioStreamPlayer _engineDemoStart = null;

	[GDEReady("CenterContainer/EngineDemoHigh", EGDEResearchMode.Path)]
	private AudioStreamPlayer _engineDemoHigh = null;

	[GDEReady("CenterContainer/EnvironmentDemo", EGDEResearchMode.Path)]
	private AudioStreamPlayer _environmentDemo = null;

	[GDEReady("CenterContainer/Music", EGDEResearchMode.Path)]
	private AudioStreamPlayer _music = null;

	#endregion

	#region Display Nodes

	[GDEReady("ParticleDemo", EGDEResearchMode.Descendant)]
	private Particles2D _particlesDemo = null;

	[GDEReady("Particles", EGDEResearchMode.Descendant)]
	private HSlider _particle = null;

	[GDEReady("StopEmittingParticles", EGDEResearchMode.Descendant)]
	private CheckButton _stopEmitting = null;

	[GDEReady("Localization", EGDEResearchMode.Descendant)]
	private OptionButton _localizations = null;

	[GDEReady("Maximized", EGDEResearchMode.Descendant)]
	private CheckButton _maximized = null;

	[GDEReady("FullScreen", EGDEResearchMode.Descendant)]
	private CheckButton _fullScreen = null;

	[GDEReady("Debug", EGDEResearchMode.Descendant)]
	private CheckButton _debug = null;

	#endregion

	#region Inputs Nodes

	[GDEReady("MyInputActionDisplay", EGDEResearchMode.Descendant)]
	private MyInputActionDisplay _inputDisplay = null;

	[GDEReady("ResetInputs", EGDEResearchMode.Descendant)]
	private Button _resetInputs = null;

	#endregion

	public override void _ExitTree()
	{
		Save();
		base._ExitTree();
	}

	public override void _EnterTree()
	{
		base._EnterTree();
		if (!_isInitialized)
		{
			_isInitialized = true;
			EMyProjectiles.Preload();
			MyAmmoRack.Preload();
			EMyPlanes.Preload();
			MyAmmoRack.Load();
			EMyPlanes.Load();
			//GDEDebug.Print("Debug enabled.");
			//GDEDebug.PrintWarning("Warning enabled.");
			//GDEDebug.PrintErr("Error enabled.");
		}
	}

	public override void _Ready()
	{
		base._Ready();
		GDEDebug.PrintCustomText = false;
		GDEDebug.Print("Loading menu...");
		MyGameSettings.Initialize(GetViewport());
		GDEReadyAttribute.SetUp(this);

		_startServer.Connect("button_up", this, OnStartServer);
		_connectToServer.Connect("button_up", this, OnConnect);
		_exit.Connect("button_up", this, OnExit);

		_planeColor.Connect<Color>("color_changed", this, OnPlaneColorChanged);
		_planeSelection.Connect<int>("item_selected", this, OnPlaneSelected);

		_planePreviewShader = (ShaderMaterial)_planePreview.Material.Duplicate();
		_planePreview.Material = _planePreviewShader;

		for (int i = 0; i < EMyPlanes.PlaneCount; i++)
		{
			_planeSelection.AddItem(EMyPlanes.GetNameFromId(i), i);
		}
		_planeSelection.Selected = MyGameSettings.CustomPlaneId;

		_playerName.Text = MyGameSettings.PlayerName;
		OnPlaneSelected(MyGameSettings.CustomPlaneId);
		_planeColor.Color = MyGameSettings.PlaneColor;
		OnPlaneColorChanged(_planeColor.Color);

		_ammoButtons = new OptionButton[] { _primaryAmmo, _secondaryAmmo, _thirdAmmo, _fourthAmmo };
		_primaryAmmo.Select(_primaryAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(0)));
		_secondaryAmmo.Select(_secondaryAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(1)));
		_thirdAmmo.Select(_thirdAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(2)));
		_fourthAmmo.Select(_fourthAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(3)));



		_primaryAmmo.Connect<int, int>("item_selected", this, OnAmmoChange, new Godot.Collections.Array() { 0 });
		_secondaryAmmo.Connect<int, int>("item_selected", this, OnAmmoChange, new Godot.Collections.Array() { 1 });
		_thirdAmmo.Connect<int, int>("item_selected", this, OnAmmoChange, new Godot.Collections.Array() { 2 });
		_fourthAmmo.Connect<int, int>("item_selected", this, OnAmmoChange, new Godot.Collections.Array() { 3 });

		_masterVolume.Value = MyGameSettings.MasterVolume;
		_gunShotsVolume.Value = MyGameSettings.GunShotsVolume;
		_enginesVolume.Value = MyGameSettings.EnginesVolume;
		_environmentVolume.Value = MyGameSettings.EnvironmentVolume;
		_musicsVolume.Value = MyGameSettings.MusicsVolume;
		_explosionsVolume.Value = MyGameSettings.ExplosionsVolume;

		_masterVolume.Connect<float>("value_changed", this, OnMasterVolumeChanged);
		_gunShotsVolume.Connect<float>("value_changed", this, OnGunShotsVolumeChanged);
		_enginesVolume.Connect<float>("value_changed", this, OnEnginesVolumeChanged);
		_environmentVolume.Connect<float>("value_changed", this, OnEnvironmentVolumeChanged);
		_musicsVolume.Connect<float>("value_changed", this, OnMusicsVolumeChanged);
		_explosionsVolume.Connect<float>("value_changed", this, OnExplosionsChanged);

		_enginesVolume.Connect("focus_exited", this, OnEngineUnfocus);
		_environmentVolume.Connect("focus_exited", this, OnEnvUnfocus);

		(_engineDemoStart.Stream as AudioStreamOGGVorbis).Loop = false;
		(_engineDemoHigh.Stream as AudioStreamOGGVorbis).LoopOffset = 3;

		_stopEmitting.Pressed = MyGameSettings.StopEmittingParticles;

		_particle.Value = MyGameSettings.Particle;
		_particlesDemo.Amount = Math.Max(1, 32 * MyGameSettings.Particle / 100);
		_particle.Connect("focus_entered", this, OnParicleFocus);
		_particle.Connect("focus_exited", this, OnParicleUnfocus);
		_particle.Connect<float>("value_changed", this, OnParticleValueChanged);

		foreach (var locale in TranslationServer.GetLoadedLocales().OfType<string>())
		{
			_localizations.AddItem(locale);
		}

		string savedLocale = MyGameSettings.Localization;
		TranslationServer.SetLocale(savedLocale);
		for (int i = 0; i < _localizations.GetItemCount(); i++)
		{
			if (_localizations.GetItemText(i) == savedLocale)
			{
				_localizations.Select(i);
			}
		}
		_localizations.Connect<int>("item_selected", this, OnLocalChanged);

		_maximized.Pressed = MyGameSettings.Maximized;
		_maximized.Connect<bool>("toggled", this, OnMaximizedToggled);
		_fullScreen.Pressed = MyGameSettings.FullScreen;
		_fullScreen.Connect<bool>("toggled", this, OnFullScreenToggled);

		_debug.Pressed = MyGameSettings.ShowDebug;
		_debug.Connect<bool>("toggled", this, OnDebugToggled);

		_resetInputs.Connect("pressed", this, ResetInputs);
		_inputDisplay.ReBuild();

		var log = this.GetDescendant<Label>("Log");
		var logAnim = this.GetDescendant<AnimationPlayer>("LogAnimation");
		log.Text = _message;
		logAnim.Play("A");
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_port = _ip = _playerName = null;
			_startServer = _connectToServer = _exit = _allowConnections = null;
			_planeSelection = _primaryAmmo = _secondaryAmmo = _thirdAmmo = _fourthAmmo = null;
			_planeColor = null;
			_planePreview = null;
			_planePreviewShader = null;
			_ammoButtons?.Clear();
			_ammoButtons = null;
			_masterVolume = _enginesVolume = _gunShotsVolume = _explosionsVolume = _environmentVolume = _musicsVolume = null;
			_gunDemo = _explosionDemo = _engineDemoStart = _engineDemoHigh = _environmentDemo = _music = null;
			_particlesDemo = null;
			_particle = null;
			_stopEmitting = _debug = _fullScreen = _maximized = null; _localizations = null;
			_inputDisplay = null;
			_resetInputs = null;
		}
	}

	#region Play Methods

	private void OnExit()
	{
		GetTree().Quit();
	}

	private void OnAmmoChange(int id, int index)
	{
		MyGameSettings.SetAmmo(_planeSelection.GetSelectedId(), index, _ammoButtons[index].GetItemId(id));
	}

	private void Save()
	{
		GDEDebug.Print("Updating settings...");
		MyGameSettings.LoadInputsToConfigs();
		MyGameSettings.Particle = (int)_particle.Value;
		MyGameSettings.StopEmittingParticles = _stopEmitting.Pressed;
		MyGameSettings.Maximized = _maximized.Pressed;
		MyGameSettings.FullScreen = _fullScreen.Pressed;
		MyGameSettings.PlayerName = _playerName.Text;
		MyGameSettings.PlaneColor = new Color(_planeColor.Color.r, _planeColor.Color.g, _planeColor.Color.b);
		MyGameSettings.CustomPlaneId = (sbyte)_planeSelection.GetSelectedId();
		MyGameSettings.SaveSettings();
	}

	private void LoadNetwork(MyNetworkManager network)
	{
		string name = _playerName.Text;
		if (string.IsNullOrWhiteSpace(name))
		{
			throw new ArgumentException("Name is null or with space.");
		}
		GetParent().AddChild(network);
		GetParent().RemoveChild(this);

		QueueFree();
	}

	private void OnConnect()
	{
		try
		{
			int port = int.Parse(_port.Text);
			string ip = _ip.Text;
			var client = new MyClientManager
			{
				Port = port,
				Ip = ip
			};
			LoadNetwork(client);
			if (client.ConnectToServer() != Error.Ok)
			{
				client.StopNetworking("ERR_CLI_START");
			}
		}
		catch (Exception e)
		{
			GDEDebug.PrintErr(e);
		}
	}

	private void OnStartServer()
	{
		try
		{
			int port = int.Parse(_port.Text);
			string name = _playerName.Text;
			if (string.IsNullOrWhiteSpace(name))
			{
				throw new ArgumentException("Name is null or with space.");
			}
			var serv = MyServerManager.Instance(port, _allowConnections.Pressed);
			LoadNetwork(serv);
			if (serv.CreateServer() != Error.Ok)
			{
				serv.StopNetworking("ERR_SERV_START");
			}
		}
		catch (Exception e)
		{
			GDEDebug.PrintErr(e);
		}

	}
	#endregion

	#region Hangar Methods
	private void OnPlaneColorChanged(Color color)
	{
		_planePreviewShader.SetShaderParam("ColorAlpha", color);
	}

	private void OnPlaneSelected(int id)
	{
		_planePreviewShader.SetShaderParam("Texture", EMyPlanes.GetPreviewFromId(id));
		var ammos = EMyPlanes.GetAviableAmmoRackFromId(id);
		_primaryAmmo.Items = new Godot.Collections.Array();
		_secondaryAmmo.Items = new Godot.Collections.Array();
		_thirdAmmo.Items = new Godot.Collections.Array();
		_fourthAmmo.Items = new Godot.Collections.Array();
		for (int i = 0; i < ammos[0].Length; i++)
		{
			_primaryAmmo.AddItem(ammos[0][i].Name, ammos[0][i].Id);
		}
		for (int i = 0; i < ammos[1].Length; i++)
		{
			_secondaryAmmo.AddItem(ammos[1][i].Name, ammos[1][i].Id);
		}
		for (int i = 0; i < ammos[2].Length; i++)
		{
			_thirdAmmo.AddItem(ammos[2][i].Name, ammos[2][i].Id);
		}
		for (int i = 0; i < ammos[3].Length; i++)
		{
			_fourthAmmo.AddItem(ammos[3][i].Name, ammos[3][i].Id);
		}
		MyGameSettings.CustomPlaneId = (sbyte)id;
		_primaryAmmo.Select(_primaryAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(0)));
		_secondaryAmmo.Select(_secondaryAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(1)));
		_thirdAmmo.Select(_thirdAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(2)));
		_fourthAmmo.Select(_fourthAmmo.GetItemIndex(MyGameSettings.GetAmmoCurrentPlane(3)));
	}

	#endregion

	#region Volume Methods

	private void Check(AudioStreamPlayer player)
	{
		if (!player.Playing)
		{
			player.Play();
		}
	}

	private void OnMasterVolumeChanged(float volume)
	{
		MyGameSettings.MasterVolume = volume;
		Check(_music);
		_masterVolume.GrabFocus();
	}

	private void OnGunShotsVolumeChanged(float volume)
	{
		MyGameSettings.GunShotsVolume = volume;
		_gunDemo.Play();
		_gunShotsVolume.GrabFocus();
	}

	private void OnEnginesVolumeChanged(float volume)
	{
		MyGameSettings.EnginesVolume = volume;
		if (!_engineDemoStart.Playing && !_engineDemoHigh.Playing)
		{
			_engineDemoStart.Play();
		}
		_enginesVolume.GrabFocus();
	}

	private void OnExplosionsChanged(float volume)
	{
		MyGameSettings.ExplosionsVolume = volume;
		Check(_explosionDemo);
		_explosionsVolume.GrabFocus();
	}

	private void OnEnvironmentVolumeChanged(float volume)
	{
		MyGameSettings.EnvironmentVolume = volume;
		Check(_environmentDemo);
		_environmentVolume.GrabFocus();
	}


	private void OnMusicsVolumeChanged(float volume)
	{
		MyGameSettings.MusicsVolume = volume;
		Check(_music);
		_musicsVolume.GrabFocus();
	}

	private void OnEnvUnfocus()
	{
		_environmentDemo.Stop();
	}

	private void OnEngineUnfocus()
	{
		_engineDemoStart.Stop();
		_engineDemoHigh.Stop();
	}

	#endregion

	#region Display Methods
	private void OnParticleValueChanged(float value)
	{
		_particlesDemo.Amount = Math.Max(1, 32 * (int)value / 100);
		_particle.GrabFocus();
	}

	private void OnParicleFocus()
	{
		_particlesDemo.Emitting = true;
	}

	private void OnParicleUnfocus()
	{
		_particlesDemo.Emitting = false;
	}

	private void OnMaximizedToggled(bool t)
	{
		MyGameSettings.Maximized = t;
	}

	private void OnFullScreenToggled(bool t)
	{
		MyGameSettings.FullScreen = t;
	}

	private void OnDebugToggled(bool b)
	{
		MyGameSettings.ShowDebug = b;
	}

	private void OnLocalChanged(int index)
	{
		string locale = _localizations.GetItemText(index);
		if (TranslationServer.GetLoadedLocales().Contains(locale))
		{
			TranslationServer.SetLocale(_localizations.GetItemText(index));
			MyGameSettings.Localization = _localizations.GetItemText(index);
			_inputDisplay.ReBuild();
		}
	}
	#endregion

	#region Inputs Methods

	private void ResetInputs()
	{
		InputMap.LoadFromGlobals();
		_inputDisplay.ReBuild();
	}

	#endregion
}
