using Godot;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;

public class MyInputActionDisplay : Tree
{


	private static readonly Texture _editIcon = GD.Load<Texture>("res://Content/Textures/Edit_Icon.png");
	private static readonly Texture _deleteIcon = GD.Load<Texture>("res://Content/Textures/Delete_Icon.png");
	private static readonly Texture _addIcon = GD.Load<Texture>("res://Content/Textures/Add_Icon.png");

	private Dictionary<string, TreeItem> _actionToSection = new Dictionary<string, TreeItem>();
	private Dictionary<TreeItem, (string action, InputEvent input)> _entryToActionEvent = new Dictionary<TreeItem, (string action, InputEvent input)>();
	private Dictionary<TreeItem, string> _sectionToAction = new Dictionary<TreeItem, string>();

	[GDEReady("Popup")]
	private PopupPanel _popup = null;

	[GDEReady("Popup/VBoxContainer/Key")]
	private Label _label = null;

	[GDEReady("Popup/VBoxContainer/HBoxContainer/Cancel")]
	private Button _cancel = null;

	[GDEReady("Popup/VBoxContainer/HBoxContainer/Ok")]
	private Button _ok = null;


	private bool _editing = false;
	private (string action, InputEvent input, TreeItem entry) _edition;
	private InputEvent _newInput = null;

	public override void _Ready()
	{
		base._Ready();
		GDEReadyAttribute.SetUp(this);
		this.Connect<TreeItem, int, int>("button_pressed", this, ButtonPressed);
		_ok.Connect("pressed", this, Ok);
		_cancel.Connect("pressed", this, Cancel);
		_popup.Connect("popup_hide", this, PopupHidden);
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_actionToSection.Clear();
			_actionToSection = null;
			_entryToActionEvent.Clear();
			_entryToActionEvent = null;
			_sectionToAction.Clear();
			_sectionToAction = null;
			_popup = null;
			_label = null;
			_cancel = _ok = null;
			_newInput = null;
		}
	}

	public void ReBuild()
	{
		_actionToSection.Clear();
		_entryToActionEvent.Clear();
		_sectionToAction.Clear();
		GetRoot()?.Free();
		var root = CreateItem();
		HideRoot = true;
		foreach (var action in InputMap.GetActions().OfType<string>().Sorted((a, b) => {
			int i = a.Substring(0, 3).CompareTo(b.Substring(0, 3));
			return i == 0 ? Tr(a).CompareTo(Tr(b)) : i; 
		}))
		{
			var section = CreateItem(root);
			section.SetText(0, Tr(action));
			section.AddButton(0, _addIcon, 0);
			_actionToSection.Add(action, section);
			_sectionToAction.Add(section, action);
			foreach (var input in InputMap.GetActionList(action).OfType<InputEvent>())
			{
				Add(action, input);
			}
		}
	}

	private bool TryFindName(InputEvent input, out string value)
	{
		if (input is null)
		{
			value = "None";
			return true;
		}
		else if (input is InputEventJoypadButton joy)
		{
			value= ((JoystickList)joy.ButtonIndex).ToString();
			return true;
		}
		else if (input is InputEventMouseButton mou)
		{
			value = string.Concat(mou.Control ? "Ctrl + " : "", mou.Alt ? "Alt + " : "", mou.Shift ? "Shift + " : "", ((ButtonList)mou.ButtonIndex).ToString());
			return true;
		}
		else if (input is InputEventKey key)
		{
			value = OS.GetScancodeString(key.Scancode);
			return true;
		} else if (input is InputEventJoypadMotion joymo)
		{
			value = ((JoystickList)joymo.Axis).ToString() + (joymo.AxisValue < 0 ? " -" : " +");
			return true;
		}
		else
		{
			value = "(Unsupported) " + input.AsText();
			return false;
		}
	}
	
	private void Cancel()
	{
		_popup.Hide();
	}

	private void Ok()
	{
		if (_newInput != null && TryFindName(_newInput, out string name))
		{
			if (_edition.input != null)
			{
				InputMap.ActionEraseEvent(_edition.action, _edition.input);
			}
			InputMap.ActionAddEvent(_edition.action, _newInput);
			_edition.entry.SetText(0, name);
			_entryToActionEvent[_edition.entry] = (_edition.action, _newInput);
		}
		_popup.Hide();
	}

	private void PopupHidden()
	{
		_editing = false;
		_label.Text = "Enter a key...";
		_newInput = null;
	}

	public override void _UnhandledInput(InputEvent input)
	{
		base._UnhandledInput(input);
		if (_editing && TryFindName(input, out string name))
		{
			_label.Text = name;
			_newInput = input;
		}
	}

	private void Add(string action, InputEvent input)
	{
		var entry = CreateItem(_actionToSection[action]);
		TryFindName(input, out string name);
		entry.SetText(0, name);
		entry.AddButton(0, _editIcon, 0);
		entry.AddButton(0, _deleteIcon, 1);
		_entryToActionEvent[entry] = (action, input);
	}

	private void ButtonPressed(TreeItem entry, int column, int id)
	{
		if (_entryToActionEvent.TryGetValue(entry, out var tupple))
		{
			if (id == 0)
			{
				_newInput = null;
				_editing = true;
				_edition = (tupple.action, tupple.input, entry);
				_popup.PopupCentered();
			}
			else if (id == 1)
			{
				if (tupple.input != null)
				{
					InputMap.ActionEraseEvent(tupple.action, tupple.input);
				}
				entry.Free();
			}
		}
		else if (_sectionToAction.TryGetValue(entry, out var action))
		{
			Add(action, null);
		}
	}
}
