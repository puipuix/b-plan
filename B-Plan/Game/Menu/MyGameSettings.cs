﻿using Godot;
using Godot.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using GDExtension;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.Common.Projectiles;
using Array = Godot.Collections.Array;

namespace BPlan.Game.Menu
{
    public static class MyGameSettings
    {
        private const string SettingsFile = "user://settings.cfg";

        private static ConfigFile _singletonConfig;
        private static MyGameSettingsSingleton _singletonNode;
        private static Viewport _viewport;

        private class MyGameSettingsSingleton : Node
        {
            public void OnViewPortChanged()
            {
                _singletonConfig.SetValue("Display", nameof(Maximized), OS.WindowMaximized);
                _singletonConfig.SetValue("Display", nameof(FullScreen), OS.WindowFullscreen);
                CheckParent();
            }

            public void CheckParent()
            {
                if (_singletonNode.GetParentOrNull<Viewport>() != _viewport)
                {
                    _singletonNode.GetParentOrNull<Node>()?.RemoveChild(this);
                    _viewport.CallDeferred("add_child", this);
                }
            }

            public override void _EnterTree()
            {
                base._EnterTree();
                GDEDebug.Print("Setting's singleton has been  added to the viewport.");
            }

            public override void _ExitTree()
            {
                base._ExitTree();
                SaveSettings();
            }
        }

        public static void Initialize(Viewport viewport)
        {
            if (_singletonConfig is null)
            {
                _singletonConfig = new ConfigFile();
                _singletonNode = new MyGameSettingsSingleton();
                _viewport = viewport;
                viewport.Connect("size_changed", _singletonNode, _singletonNode.OnViewPortChanged);
                LoadSettings();
                Apply();
            }
        }

        #region Play

        public static string PlayerName
        {
            get => _singletonConfig.GetValue<string>("Play", nameof(PlayerName), "Player");
            set => _singletonConfig.SetValue("Play", nameof(PlayerName), value);
        }

        #endregion

        #region Hangar

        public static Color PlaneColor
        {
            get => _singletonConfig.GetValue<Color>("Hangar", nameof(PlaneColor), Colors.Red);
            set => _singletonConfig.SetValue("Hangar", nameof(PlaneColor), value);
        }

        public static sbyte CustomPlaneId
        {
            get => (sbyte)_singletonConfig.GetValue<int>("Hangar", nameof(CustomPlaneId), EMyPlanes.Biplan);
            set => _singletonConfig.SetValue("Hangar", nameof(CustomPlaneId), value);
        }

        private static string PlaneSection(int plane) => "Hangar/" + EMyPlanes.GetNameFromId(plane);

        private static string[] keyIndex = { "Primary", "Secondary", "Third", "Fourth" };


        public static int GetAmmo(int plane, int index)
        {
            return _singletonConfig.GetValue(PlaneSection(plane), keyIndex[index], () => EMyPlanes.GetAviableAmmoRackFromId(plane)[index][0].Id);
        }

        public static void SetAmmo(int plane, int index, int ammo)
        {
            _singletonConfig.SetValue(PlaneSection(plane), keyIndex[index], ammo);
        }

        public static int GetAmmoCurrentPlane(int index)
        {
            return GetAmmo(CustomPlaneId, index);
        }

        public static void SetAmmoCurrentPlane(int index, int ammo)
        {
            SetAmmo(CustomPlaneId, index, ammo);
        }

        public static int[][][] Ammos
        {
            get {
                var tab = new int[EMyPlanes.PlaneCount][][];
                for (int i = 0; i < EMyPlanes.PlaneCount; i++)
                {
                    tab[i] = new int[4][];
                    for (int j = 0; j < 4; j++)
                    {
                        tab[i][j] = MyAmmoRack.GetFromId(GetAmmo(i, j));
                    }
                }
                return tab;
            }
        }

        #endregion

        #region Volumes

        public static float MasterVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(MasterVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(MasterVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Master"), value);
            }
        }

        public static float EnginesVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(EnginesVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(EnginesVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Engines"), EnginesVolume);
            }
        }

        public static float GunShotsVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(GunShotsVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(GunShotsVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("GunShots"), GunShotsVolume);
            }
        }

        public static float ExplosionsVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(ExplosionsVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(ExplosionsVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Explosions"), ExplosionsVolume);
            }
        }

        public static float EnvironmentVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(EnvironmentVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(EnvironmentVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Environment"), EnvironmentVolume);
            }
        }


        public static float MusicsVolume
        {
            get => _singletonConfig.GetValue<float>("Volume", nameof(MusicsVolume), 0);
            set {
                _singletonConfig.SetValue("Volume", nameof(MusicsVolume), value);
                AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Musics"), MusicsVolume);
            }
        }

        #endregion

        #region Display

        public static int Particle
        {
            get => _singletonConfig.GetValue<int>("Display", nameof(Particle), 50);
            set => _singletonConfig.SetValue("Display", nameof(Particle), value);
        }

        public static bool StopEmittingParticles
        {
            get => _singletonConfig.GetValue<bool>("Display", nameof(StopEmittingParticles), true);
            set => _singletonConfig.SetValue("Display", nameof(StopEmittingParticles), value);
        }

        public static string Localization
        {
            get => _singletonConfig.GetValue<string>("Display", nameof(Localization), TranslationServer.GetLocale());
            set {
                _singletonConfig.SetValue("Display", nameof(Localization), value);
            }
        }

        public static bool FullScreen
        {
            get => _singletonConfig.GetValue<bool>("Display", nameof(FullScreen), false);
            set {
                _singletonConfig.SetValue("Display", nameof(FullScreen), value);
                ApplyWindow();
            }
        }

        public static bool Maximized
        {
            get => _singletonConfig.GetValue<bool>("Display", nameof(Maximized), false);
            set {
                _singletonConfig.SetValue("Display", nameof(Maximized), value);
                ApplyWindow();
            }
        }

        public static bool ShowDebug
        {
            get => _singletonConfig.GetValue<bool>("Display", nameof(ShowDebug), false);
            set {
                _singletonConfig.SetValue("Display", nameof(ShowDebug), value);
            }
        }

        #endregion

        #region Controls

        private static void SetModifiersFromRaw(InputEventWithModifiers input, Dictionary raw)
        {
            input.Alt = (bool)raw["Alt"];
            input.Control = (bool)raw["Control"];
            input.Command = (bool)raw["Command"];
            input.Shift = (bool)raw["Shift"];
            input.Meta = (bool)raw["Meta"];
        }

        private static void SetModifiersToRaw(InputEventWithModifiers input, Dictionary raw)
        {
            raw["Alt"] = input.Alt;
            raw["Control"] = input.Control;
            raw["Command"] = input.Command;
            raw["Shift"] = input.Shift;
            raw["Meta"] = input.Meta;
        }

        public static void LoadInputsFromConfigs()
        {
            if (_singletonConfig.HasSection("Inputs"))
            {
                foreach (var action in _singletonConfig.GetSectionKeys("Inputs"))
                {
                    InputMap.EraseAction(action);
                    InputMap.AddAction(action);
                    var rawArray = (Array)_singletonConfig.GetValue("Inputs", action, new Array());
                    foreach (var raw in rawArray.OfType<Dictionary>())
                    {
                        string type = (string)raw["Type"];
                        if (type == nameof(InputEventJoypadButton))
                        {
                            var jb = new InputEventJoypadButton()
                            {
                                ButtonIndex = (int)raw["ButtonIndex"],
                            };
                            InputMap.ActionAddEvent(action, jb);
                        }
                        else if (type == nameof(InputEventJoypadMotion))
                        {
                            var jm = new InputEventJoypadMotion()
                            {
                                Axis = (int)raw["Axis"],
                                AxisValue = (float)raw["AxisValue"],
                            };
                            InputMap.ActionAddEvent(action, jm);
                        }
                        else if (type == nameof(InputEventMouseButton))
                        {
                            var mb = new InputEventMouseButton()
                            {
                                ButtonIndex = (int)raw["ButtonIndex"],
                                ButtonMask = (int)raw["ButtonMask"],
                            };
                            SetModifiersFromRaw(mb, raw);
                            InputMap.ActionAddEvent(action, mb);
                        }
                        else if (type == nameof(InputEventKey))
                        {
                            var key = new InputEventKey()
                            {
                                Scancode = (uint)(int)raw["Scancode"]
                            };
                            SetModifiersFromRaw(key, raw);
                            InputMap.ActionAddEvent(action, key);
                        }
                    }
                }
            }
        }

        public static void LoadInputsToConfigs()
        {
            foreach (var action in InputMap.GetActions().OfType<string>())
            {
                var rawArray = new Array();
                foreach (var input in InputMap.GetActionList(action).OfType<InputEvent>())
                {
                    var raw = new Dictionary();
                    if (input is InputEventJoypadButton jb)
                    {
                        raw["Type"] = nameof(InputEventJoypadButton);
                        raw["ButtonIndex"] = jb.ButtonIndex;
                    }
                    else if (input is InputEventJoypadMotion jm)
                    {
                        raw["Type"] = nameof(InputEventJoypadMotion);
                        raw["Axis"] = jm.Axis;
                        raw["AxisValue"] = jm.AxisValue;
                    }
                    else if (input is InputEventMouseButton mb)
                    {
                        raw["Type"] = nameof(InputEventMouseButton);
                        raw["ButtonIndex"] = mb.ButtonIndex;
                        raw["ButtonMask"] = mb.ButtonMask;
                        SetModifiersToRaw(mb, raw);
                    }
                    else if (input is InputEventKey key)
                    {
                        raw["Type"] = nameof(InputEventKey);
                        raw["Scancode"] = (int)key.Scancode;
                        SetModifiersToRaw(key, raw);
                    }
                    else
                    {
                        raw["Type"] = "(Unsupported) " + input.GetType().Name;
                    }
                    rawArray.Add(raw);
                }
                _singletonConfig.SetValue("Inputs", action, rawArray);
            }
        }

        #endregion

        public static void LoadSettings()
        {
            GDEDebug.Print("Loading '" + SettingsFile + "'...");
            Error err = _singletonConfig.Load(SettingsFile);
            if (err == Error.FileNotFound)
            {
                GDEDebug.Print("\tFile not found.");
            }
            else if (err != Error.Ok)
            {
                GDEDebug.PrintErr("\tError: '" + err.ToString() + "'");
                GDEDebug.PrintErr("\tDeleting '" + SettingsFile + "'...");
                GDEDebug.PrintErr("\t\t" + new Directory().Remove(SettingsFile).ToString());
            }
        }

        public static void SaveSettings()
        {
            GDEDebug.Print("Saving '" + SettingsFile + "'...");
            Error err = _singletonConfig.Save(SettingsFile);
            if (err != Error.Ok)
            {
                GDEDebug.PrintErr("\t" + err.ToString());
            }
        }

        public static void Apply()
        {
            LoadInputsFromConfigs();

            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Master"), MasterVolume);
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Engines"), EnginesVolume);
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("GunShots"), GunShotsVolume);
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Explosions"), ExplosionsVolume);
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Musics"), MusicsVolume);
            AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex("Environment"), EnvironmentVolume);

            ApplyWindow();
        }

        private static void ApplyWindow()
        {
            _singletonNode.CheckParent();
            OS.WindowMaximized = Maximized;
            OS.WindowFullscreen = FullScreen;
        }
    }
}
