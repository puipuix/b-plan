﻿using BPlan.Game.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDExtension;
using BPlan.Game.NetWork;
using Godot;
using BPlan.Game.Common.Vehicles.Planes;
using GDExtension.Binaries;

namespace BPlan.Game.Tests
{
    public static class MyTestRPC
    {
        public static void Test()
        {
            new MyGameRule(new MyGameRule(true, true, true, true, true).ToBinaryBuffer());
            MyIdentification id = new MyIdentification(new MyIdentification(32, 1, 0).ToBinaryBuffer());
            new MyPlaneData(new MyPlaneData(id.ServerPlaneId, id, EMyPlayerInputType.Local, Colors.RebeccaPurple, new Transform2D(), false).ToBinaryBuffer());
            new MyProjectileData(new MyProjectileData(0, id, new Vector2(), new Vector2(), new Vector2(), 0, "ammo test", 1.1f).ToBinaryBuffer());
            new MyScenarioData(new MyScenarioData(new string[] { "Alpha", "Bravo" }).ToBinaryBuffer());
            new MyClientData(new MyClientData(1,"user test", 0, EMyPlanes.GetRandomAmmoRacks(), Colors.Red).ToBinaryBuffer());
            var log = new MyPlayerNameLogText(id, "test") + " bla";
            MyLogText.ReadFromBinaryBuffer(log.ToBinaryBuffer());
        }
    }
}
