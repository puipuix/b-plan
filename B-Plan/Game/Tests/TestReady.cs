using BPlan.Game.Tests;
using Godot;
using GDExtension;
using System;

public class TestReady : TestReadyParent
{
    [GDEReady("Control2")]
    protected Control c2 = null;

    public override void _Ready()
    {
        GDEDebug.Print("before base: " + c1.SafeToString() + " " + c2.SafeToString());
        base._Ready();
        GDEDebug.Print("after base: " + c1.SafeToString() + " " + c2.SafeToString());
        GDEDebug.Print("Set value to null");
        c1 = c2 = null;

        GDEDebug.Print("before setup: " + c1.SafeToString() + " " + c2.SafeToString());
        GDEReadyAttribute.SetUp(this);
        GDEDebug.Print("after setup: " + c1.SafeToString() + " " + c2.SafeToString());
    }
}
