﻿using Godot;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Tests
{
    public class TestReadyParent : Control
    {
        [GDEReady("Control")]
        protected Control c1 = null;

        public override void _Ready()
        {
            base._Ready();
            GDEReadyAttribute.SetUp(this);
        }
    }
}
