﻿using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

namespace BPlan.Game.NetWork
{
    public struct MyScenarioData : IGDEBinaryData
    {
        public string[] TeamNames { get; private set; }

        public int TeamCount => TeamNames.Length;

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out string[] names, GDEBinaryConverter.Read, buffer, index);
            TeamNames = names;
            return index;
        }

        public MyScenarioData(string[] teamNames)
        {
            TeamNames = teamNames;
        }

        public MyScenarioData(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }
        public MyScenarioData(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            return GDEBinaryConverter.Write(TeamNames, GDEBinaryConverter.Write, buffer, index);
        }
        
        public int GetBinaryBufferLength()
        {
            return GDEBinaryConverter.GetArrayBufferLength(TeamNames);
        }
    }
}
