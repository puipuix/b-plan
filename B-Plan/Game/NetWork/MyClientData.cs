﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Array = Godot.Collections.Array;
using GDExtension.Binaries;

namespace BPlan.Game.NetWork
{
    public struct MyClientData : IGDEBinaryData
    {
        public int NetworkId { get; private set; }
        public string UserName { get; private set; }
        public bool IsNPC { get; private set; }

        public int[][][] Ammos { get; private set; }
        public sbyte CustomPlaneId { get; private set; }
        public Color CustomColor { get; private set; }

        public MyClientData(int networkId, string userName, sbyte customPlaneId, int[][][] ammos, Color custom, bool isNPC = false)
        {
            NetworkId = networkId;
            UserName = userName;
            IsNPC = isNPC;
            CustomPlaneId = customPlaneId;
            CustomColor = custom;
            Ammos = ammos;
        }

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out int n, buffer, index);
            NetworkId = n;
            index = GDEBinaryConverter.Read(out string na, buffer, index);
            UserName = na;
            index = GDEBinaryConverter.Read(out bool bo, buffer, index);
            IsNPC = bo;
            index = GDEBinaryConverter.Read(out sbyte p, buffer, index);
            CustomPlaneId = p;
            index = GDEBinaryConverter.Read(out Color c, buffer, index);
            CustomColor = c;
            index = GDEBinaryConverter.Read<int[][]>(out int[][][] tabtabtab, (out int[][] tabtab, byte[] buf, int i) =>
            {
                return GDEBinaryConverter.Read<int[]>(out tabtab, (out int[] tab, byte[] b, int j) =>
                {
                    return GDEBinaryConverter.Read<int>(out tab, GDEBinaryConverter.Read, b, j);
                }, buf, i);
            }, buffer, index);
            Ammos = tabtabtab;
            return index;
        }

        public MyClientData(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }

        public MyClientData(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public int[] GetPrimaryAmmo(int planeId) => Ammos[planeId][0];
        public int[] GetSecondaryAmmo(int planeId) => Ammos[planeId][1];
        public int[] GetThirdAmmo(int planeId) => Ammos[planeId][2];
        public int[] GetFourthAmmo(int planeId) => Ammos[planeId][3];

        public int GetBinaryBufferLength()
        {
            return GDEBinaryConverter.GetTotalBufferLength(NetworkId, UserName, IsNPC, Ammos, CustomPlaneId, CustomColor);
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(NetworkId, buffer, index);
            index = GDEBinaryConverter.Write(UserName, buffer, index);
            index = GDEBinaryConverter.Write(IsNPC, buffer, index);
            index = GDEBinaryConverter.Write(CustomPlaneId, buffer, index);
            index = GDEBinaryConverter.Write(CustomColor, buffer, index);
            index = GDEBinaryConverter.Write<int[][]>(Ammos, (tabtab, buf, i) =>
            {
                return GDEBinaryConverter.Write<int[]>(tabtab, (tab, b, j) =>
                {
                    return GDEBinaryConverter.Write<int>(tab, GDEBinaryConverter.Write, b, j);
                }, buf, i);
            }, buffer, index);
            return index;
        }
    }
}
