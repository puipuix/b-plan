﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

namespace BPlan.Game.NetWork
{
    public struct MyPlaneData : IGDEBinaryData
    {
        public int PlaneId { get; private set; }
        public MyIdentification Identification { get; private set; }
        public EMyPlayerInputType InputType { get; private set; }
        public Color Color { get; private set; }
        public Transform2D Transform { get; private set; }
        public bool Reversed { get; private set; }

        public MyPlaneData(int planeId, MyIdentification identification, EMyPlayerInputType inputType, Color color, Transform2D transform, bool reversed)
        {
            PlaneId = planeId;
            Identification = identification;
            InputType = inputType;
            Color = color;
            Transform = transform;
            Reversed = reversed;
        }

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out int p, buffer, index);
            PlaneId = p;
            index = GDEBinaryConverter.Read(out MyIdentification i, buffer, index);
            Identification = i;
            index = GDEBinaryConverter.Read(out byte b, buffer, index);
            InputType = (EMyPlayerInputType)b;
            index = GDEBinaryConverter.Read(out Color c, buffer, index);
            Color = c;
            index = GDEBinaryConverter.Read(out Transform2D t, buffer, index);
            Transform = t;
            index = GDEBinaryConverter.Read(out bool r, buffer, index);
            Reversed = r;
            return index;
        }

        public MyPlaneData(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }

        public MyPlaneData(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public int GetBinaryBufferLength()
        {
            return GDEBinaryConverter.GetTotalBufferLength(PlaneId, Identification, Color, Transform, Reversed) + 1;
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(PlaneId, buffer, index);
            index = GDEBinaryConverter.Write(Identification, buffer, index);
            index = GDEBinaryConverter.Write((byte)InputType, buffer, index);
            index = GDEBinaryConverter.Write(Color, buffer, index);
            index = GDEBinaryConverter.Write(Transform, buffer, index);
            index = GDEBinaryConverter.Write(Reversed, buffer, index);
            return index;
        }
    }
}
