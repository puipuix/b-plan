﻿using BPlan.Game.Common;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

namespace BPlan.Game.NetWork
{
    public struct MyProjectileData : IGDEBinaryData
    {
        public int ProjectileID { get; set; }
        public MyIdentification Identification { get; set; }
        public Vector2 Position { get; set; }
        public Vector2 Velocity { get; set; }
        public Vector2 Direction { get; set; }
        public int GunDamage { get; set; }
        public string Name { get; set; }
        public float Scale { get; set; }

        public MyProjectileData(int projectileID, MyIdentification identification, Vector2 position, Vector2 velocity, Vector2 direction, int gundmg, string name, float scale)
        {
            ProjectileID = projectileID;
            Identification = identification;
            Position = position;
            Velocity = velocity;
            Direction = direction;
            GunDamage = gundmg;
            Name = name;
            Scale = scale;
        }

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out int pro, buffer, index);
            ProjectileID = pro;
            index = GDEBinaryConverter.Read(out MyIdentification i, buffer, index);
            Identification = i;
            index = GDEBinaryConverter.Read(out Vector2 po, buffer, index);
            Position = po;
            index = GDEBinaryConverter.Read(out Vector2 v, buffer, index);
            Velocity = v;
            index = GDEBinaryConverter.Read(out Vector2 d, buffer, index);
            Direction = d;
            index = GDEBinaryConverter.Read(out int g, buffer, index);
            GunDamage = g;
            index = GDEBinaryConverter.Read(out string n, buffer, index);
            Name = n;
            index = GDEBinaryConverter.Read(out float s, buffer, index);
            Scale = s;
            return index;
        }

        public MyProjectileData(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }

        public MyProjectileData(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public int GetBinaryBufferLength()
        {
            return GDEBinaryConverter.GetTotalBufferLength(ProjectileID, Identification, Position, Velocity, Direction, GunDamage, Name, Scale);
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(ProjectileID, buffer, index);
            index = GDEBinaryConverter.Write(Identification, buffer, index);
            index = GDEBinaryConverter.Write(Position, buffer, index);
            index = GDEBinaryConverter.Write(Velocity, buffer, index);
            index = GDEBinaryConverter.Write(Direction, buffer, index);
            index = GDEBinaryConverter.Write(GunDamage, buffer, index);
            index = GDEBinaryConverter.Write(Name, buffer, index);
            index = GDEBinaryConverter.Write(Scale, buffer, index);
            return index;
        }
        
    }
}
