using BPlan.Game.Common;
using BPlan.Game.Common.Components;
using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.Menu;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.CodeDom;
using System.Linq;
using GDExtension.Binaries;

//[Tool]
public class MyComponent : Area2D
{
    private const int BaseTextureIndex = 0, DamagedTextureIndex = 1, DestroyedTextureIndex = 2;

    [Export]
    public int MaxHeal { get; set; } = 1000;

    [Export]
    public float MaxFunctionalHeal { get; set; } = 0.75f;

    [Export]
    public float ExplosionChancePerDamage { get; set; } = 0;

    [Export]
    public int ExplosionRadius { get; set; } = 25;

    [Export]
    public bool CanBurn { get; set; }

    [Export]
    public bool CanBurnWhenDestroyed { get; set; }

    [Export]
    public bool CanSmoke { get; set; }

    [Export]
    public float SmokeScale { get; set; } = 1;

    [Export]
    public int FireDamage { get; set; } = 10;

    [Export]
    public float FireScale { get; set; } = 1;

    [Export]
    public EMyComponentType ComponentType { get; set; } = 0;

    [Export]
    public bool CanTakeCollisionDamage { get; set; } = true;

    [Export(PropertyHint.ResourceType, "Texture")]
    public Texture BaseTexture { get; set; }

    [Export(PropertyHint.ResourceType, "Texture")]
    public Texture DamagedTexture { get; set; }

    [Export(PropertyHint.ResourceType, "Texture")]
    public Texture DestroyedTexture { get; set; }

    [Puppet]
    private int _heal = 0;

    [Export]
    public int Heal
    {
        get => _heal;
        set {
            _heal = value;
            if (IsInsideTree() && GetTree().HasNetworkPeer() && IsNetworkMaster())
            {
                Rset(nameof(_heal), _heal);
            }
        }
    }

    public bool IsOnFire => _burnDuration > 0 || (IsDestroyed && CanBurnWhenDestroyed);

    public float Effectiveness => Mathf.Clamp(Heal / (MaxFunctionalHeal * MaxHeal), 0, 1);

    public bool IsDamaged => Heal < MaxFunctionalHeal * MaxHeal;
    public bool IsDestroyed => Heal <= 0;
    public bool HasScratch => Heal < MaxHeal;

    private Color _color;
    public Color Color
    {
        get => _color;
        set {
            _color = value;
            if (Sprite?.Material is ShaderMaterial material)
            {
                material.SetShaderParam("ColorAlpha", value);
            }
        }
    }

    [Export]
    public Array<NodePath> ToDestroyOnDestroyed { get; set; } = new Array<NodePath>();
    private Node2D[] _toDestroyOnDestroyed;

    [Export]
    public bool HideOnDestroy { get; set; } = false;

    private Vector2 _startPosition;
    private float _startRotation;

    private float _deathDuration = 0f;
    [Master]
    private float _burnDuration = 0f;
    private float _fireDamageCooldown = 0f;
    private MyIdentification? _fireOrigin = null;
    private int _currentTextureIndex = -1;
    private bool _subCompHidden = false;

    protected Particles2D _fireEmitter;
    protected Particles2D _smokeEmitter;
    public Sprite Sprite { get; private set; }

    public MyPlane Plane { get; private set; }

    private int? _lastHitMsg;
    private MyIdentification? _lastHitOrigin;

    public override void _Ready()
    {
        _startRotation = Rotation;
        _startPosition = Position;
        base._Ready();
        if (Engine.EditorHint)
        {

        }
        else
        {
            _heal = MaxHeal;
            Connect("body_entered", this, nameof(OnBodyEntered));
            Plane = this.GetAncestorOrNull<MyPlane>();
        }
        _toDestroyOnDestroyed = ToDestroyOnDestroyed.Select(p => GetNode<Node2D>(p)).ToArray();
        Sprite = GetChildren().OfType<Sprite>().First();
        Sprite.Material = (Material)Sprite.Material.Duplicate();

        UpdateTextures(true);

        _fireEmitter = GetNode<Particles2D>("FireEmitter");
        _smokeEmitter = GetNode<Particles2D>("SmokeEmitter");

        _fireEmitter.Amount = Math.Max(1, _fireEmitter.Amount * MyGameSettings.Particle / 100);
        _smokeEmitter.Amount = Math.Max(1, _smokeEmitter.Amount * MyGameSettings.Particle / 100);

        _fireEmitter.Emitting = false;
        _smokeEmitter.Emitting = false;

        _fireEmitter.ProcessMaterial = (Material)_fireEmitter.ProcessMaterial.Duplicate();
        _smokeEmitter.ProcessMaterial = (Material)_smokeEmitter.ProcessMaterial.Duplicate();

        (_fireEmitter.ProcessMaterial as ParticlesMaterial).Scale = FireScale * 2f;
        (_smokeEmitter.ProcessMaterial as ParticlesMaterial).Scale = -1;
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        BaseTexture = DamagedTexture = DestroyedTexture = null;
        ToDestroyOnDestroyed = null;
        _toDestroyOnDestroyed?.Clear();
        _toDestroyOnDestroyed = null;
        _fireEmitter = _smokeEmitter = null;
        Sprite = null;
        Plane = null;
    }

    private int GetTextureIndex()
    {
        return IsDestroyed ? DestroyedTextureIndex : (IsDamaged ? DamagedTextureIndex : BaseTextureIndex);
    }

    private int TransformMsg(int msg)
    {
        if (msg == EMyLogMessage.ShotDown)
        {
            switch (ComponentType)
            {
                case EMyComponentType.Engine:
                    return EMyLogMessage.TurnOff;
                case EMyComponentType.Tail:
                    return EMyLogMessage.TailOff;
                case EMyComponentType.Pilot:
                    return EMyLogMessage.Killed;
                default:
                    return msg;
            }
        }
        else
        {
            return msg;
        }
    }


    [PuppetSync]
    private void RPCExplode(byte[] buffer)
    {
        GDEBinaryConverter.ReadNullable(out MyIdentification? origin, buffer, 0);
        var expl = MyExplosion.Instance(ExplosionRadius, Plane.Identification, origin, 1000, 5, 10);
        Plane.GetParent().CallDeferred("add_child", expl);
        expl.CallDeferred(expl.Explode, GlobalPosition);
    }

    protected virtual bool WillExplode(int damage, bool explosiveDamage)
    {
        float percentDamage = (float)damage / MaxHeal;
        return explosiveDamage && GDERandom.RNG.NextBool(ExplosionChancePerDamage * percentDamage * percentDamage);
    }

    public void Damage(int value, int msgType, MyIdentification? origin, bool explosiveDamage = true)
    {
        if (!IsDestroyed && IsNetworkMaster() && Plane.Battle.FightEnabled)
        {
            if (WillExplode(value, explosiveDamage))
            {
                value = MaxHeal;
                msgType = origin.HasValue ? EMyLogMessage.Explosion : EMyLogMessage.SelfExplosion;
                byte[] buffer = new byte[GDEBinaryConverter.GetNullableBufferLength(origin)];
                GDEBinaryConverter.WriteNullable(origin, buffer, 0);
                this.Rpc(RPCExplode, buffer);
            }
            int damage = Math.Min(value, Heal);
            Heal -= damage;
            _lastHitMsg = msgType;

            if (origin.HasValue)
            {
                _lastHitOrigin = origin;
            }

            if (IsDestroyed)
            {
                Plane.OnComponentDestroyed(TransformMsg(_lastHitMsg ?? msgType), _lastHitOrigin);
                PropagateDestroyed();
            }
            else
            {
                Plane.OnComponentDamaged(origin, damage);
            }
        }
    }

    public void Burn(float time, MyIdentification? origin)
    {
        if (CanBurn)
        {
            Rset(nameof(_burnDuration), Math.Max(_burnDuration, time));
            if (origin.HasValue)
            {
                _fireOrigin = origin;
            }
        }
    }


    public void OnBodyEntered(Node body)
    {
        if (!IsDestroyed && IsNetworkMaster())
        {
            if (body is IMyDamageElement element)
            {
                element.DamageComponent(this);
            }
        }
    }

    public override void _PhysicsProcess(float delta)
    {
        base._PhysicsProcess(delta);
        if (Plane.Battle.FightEnabled)
        {
            if (IsNetworkMaster())
            {
                if (_burnDuration > 0)
                {
                    _burnDuration -= delta;
                    if (_fireDamageCooldown <= 0)
                    {
                        Damage(FireDamage, _fireOrigin.HasValue ? EMyLogMessage.BurnUp : EMyLogMessage.SelfBurn, _fireOrigin);
                        _fireDamageCooldown = 0.1f;
                    }
                    else
                    {
                        _fireDamageCooldown -= delta;
                    }
                }
            } else if (_burnDuration > 0)
            {
                _burnDuration -= delta;
            }

            if (IsDestroyed && _deathDuration < 20)
            {
                _deathDuration += delta;
            }
            UpdateVisual();
        }
    }

    private void UpdateVisual()
    {
        var fire = IsOnFire && (_deathDuration < 10 || !MyGameSettings.StopEmittingParticles);
        if (fire != _fireEmitter.Emitting)
        {
            _fireEmitter.Emitting = fire;
        }
        var smoke = IsDamaged && CanSmoke && (_deathDuration < 20 || !MyGameSettings.StopEmittingParticles);
        var smokeScale = 4 * SmokeScale * (1 - Effectiveness);
        if (smoke != _smokeEmitter.Emitting)
        {
            _smokeEmitter.Emitting = smoke;
        }
        var material = _smokeEmitter.ProcessMaterial as ParticlesMaterial;
        if (smoke && smokeScale != material.Scale)
        {
            material.Scale = smokeScale;
        }

        HideSubComponent();

        if (_deathDuration < 15)
        {
            UpdateTextures();
        }
    }

    private void PropagateDestroyed()
    {
        foreach (var item in _toDestroyOnDestroyed)
        {
            if (item is MyComponent component)
            {
                component.Damage(short.MaxValue, _lastHitMsg ?? EMyLogMessage.Crash, _lastHitOrigin, false);
            }
            else if (item is CollisionShape2D collision)
            {
                if (!collision.Disabled)
                {
                    collision.SetDeferred("disabled", true);
                }
            }
            else
            {
                GDEDebug.PrintWarning("The object: '" + item.Name + " of type: '" + item.GetType().Name + "' isn't implemented.");
            }
        }
    }

    private void UpdateTextures(bool force = false)
    {
        int index = GetTextureIndex();
        if (force || _currentTextureIndex != index)
        {
            _currentTextureIndex = index;
            Texture texture;
            switch (index)
            {
                case BaseTextureIndex: texture = BaseTexture; break;
                case DamagedTextureIndex: texture = DamagedTexture; break;
                case DestroyedTextureIndex: texture = DestroyedTexture; break;
                default: texture = BaseTexture; break;
            }

            if (Sprite?.Material is ShaderMaterial sh)
            {
                sh.SetShaderParam("Texture", texture);
                Sprite.Visible = texture != null;
            }
        }
    }

    private void HideSubComponent(bool force = false)
    {
        if (force || _subCompHidden != (IsDestroyed && HideOnDestroy))
        {
            _subCompHidden = IsDestroyed && HideOnDestroy;
            foreach (var item in _toDestroyOnDestroyed)
            {
                item.Visible = !_subCompHidden;
            }
        }
    }

    [Master]
    private void RPCAskState(int peer)
    {
        this.RpcId(peer, RPCSetState, Visible, Heal);
    }

    public void AskUpdateState()
    {
        this.RpcId(GetNetworkMaster(), RPCAskState, GetTree().GetNetworkUniqueId());
    }

    [PuppetSync]
    private void RPCSetState(bool visible, int heal)
    {
        _heal = heal;
        UpdateVisual();
        Visible = visible;
    }
}
