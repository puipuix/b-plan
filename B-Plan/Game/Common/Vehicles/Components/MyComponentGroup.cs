using Godot;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common.Components
{
    public enum EMyComponentType
    {
        None = 0, Wing = 1, Engine = 2, Tail = 3, Body = 4, Pilot = 5, PrimaryGun = 6, SecondaryGun = 7, ThirdGun = 8, FourthGun = 9
    }

    public enum EmyGunType { None = 0, Gun = 1, Turret = 2, Bomb = 3, Rocket = 4 }

    public class MyComponentGroup<T> : IEnumerable<T>, IDisposable where T : MyComponent
    {
        public List<T> Components { get; private set; } = new List<T>();

        public int Count => Components?.Count ?? 0;

        public void SetUp(Node node, EMyComponentType componentType)
        {
            Components = new List<T>(node.GetChildren().OfType<T>().Where(c => c.ComponentType == componentType));
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Components.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Components.GetEnumerator();
        }


        public bool AreDestroyed
        {
            get {
                bool destroyed = true;
                foreach (var item in Components)
                {
                    if (!item.IsDestroyed)
                    {
                        destroyed = false;
                        break;
                    }
                }
                return destroyed;
            }
        }

        public float TotalEffectiveness
        {
            get {
                int count = 0;
                float sum = 0;
                foreach (var item in Components)
                {
                    count++;
                    sum += item.Effectiveness;
                }
                return count > 0 ? sum / count : 0;
            }
        }
        
        public void Dispose()
        {
            Components.Clear();
            Components = null;
        }
    }
}
