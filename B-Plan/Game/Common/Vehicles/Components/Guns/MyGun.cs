using BPlan.Game.Common.Projectiles;
using BPlan.Game.NetWork;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

//[Tool]
public class MyGun : MyComponent
{
	[Export]
	public int GunDamage { get; set; } = 0;
	[Export]
	public int AmmoCount { get; set; } = 0;

	[Export(PropertyHint.Range, "1,10000")]
	public float RateOfFire { get; set; } = 1;

	[Export(PropertyHint.Range, "0, 180")]
	public float AngleOfDispertion { get; set; } = 0;

	[Export]
	public int AmmoPerShot { get; set; } = 1;

	[Export]
	public float AmmoScale { get; set; } = 1;

	[Export]
	public bool HideWhenEmpty { get; set; } = false;

	public int[] AmmoRack { get; set; }

	public float ShotCoolDown => RateOfFire < 1 ? float.MaxValue : 60 / RateOfFire;

	public int MaxAmmoCount { get; private set; }

	public Vector2 MuzzlePosition => Plane.GetParent<Node2D>().ToLocal(_ammoSpawn.GlobalPosition);

	private Position2D _ammoSpawn;
	private AudioStreamPlayer2D _audio;
	private float _shotCooldownLeft;
	private int _beltIndex;
	private long _bulletIndexId;

	public override void _Ready()
	{
		base._Ready();
		_ammoSpawn = GetChildren().OfType<Position2D>().First();
		_audio = GetChildren().OfType<AudioStreamPlayer2D>().First();
		MaxAmmoCount = AmmoCount;
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_ammoSpawn = null;
			_audio = null;
			AmmoRack = null;
		}
	}

	public virtual void Shot()
	{
		if (IsNetworkMaster() && _shotCooldownLeft <= 0 && AmmoCount > 0)
		{
			_shotCooldownLeft = ShotCoolDown;
			if (GDERandom.RNG.NextBool(Effectiveness) && AmmoRack.Length > 0)
			{
				int ammo = AmmoRack[_beltIndex = (_beltIndex + 1) % AmmoRack.Length];
				var bullets = Enumerable.Range(0, AmmoPerShot).Select(i => {
					(Vector2 direction, string id) tmp = (
					GlobalTransform.x.Rotated(Mathf.Deg2Rad(AngleOfDispertion * (GDERandom.RNG.NextFloat() * 2 - 1))),
					Plane.Name + "_" + Name + "_Pr" + _bulletIndexId++);
					return tmp;
					}).ToArray();
								

				var buffer = new byte[GDEBinaryConverter.GetBufferLength(ammo) + GDEBinaryConverter.GetArrayBufferLength(bullets, t=>GDEBinaryConverter.GetTotalBufferLength(t.direction, t.id))];
				int index = 0;
				index = GDEBinaryConverter.Write(ammo, buffer, index);
				index = GDEBinaryConverter.Write(bullets, ((Vector2 direction, string id) t, byte[] buf, int i) => {
					i = GDEBinaryConverter.Write(t.direction, buf, i);
					i = GDEBinaryConverter.Write(t.id, buf, i);
					return i;
				}, buffer, index);

				this.Rpc(RPCShot, buffer);
			}
		}
	}

	protected override bool WillExplode(int damage, bool explosiveDamage)
	{
		float percentDamage = (float)damage / MaxHeal;
		float percentAmmo = (float)AmmoCount / MaxAmmoCount;
		return explosiveDamage && GDERandom.RNG.NextBool(percentAmmo * ExplosionChancePerDamage * percentDamage * percentDamage);
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (_shotCooldownLeft > -1)
		{
			_shotCooldownLeft -= delta;
		}
	}

	[PuppetSync]
	private void RPCShot(byte[] buffer)
	{
		AmmoCount--;
		int index = 0;
		index = GDEBinaryConverter.Read(out int ammo, buffer, index);
		index = GDEBinaryConverter.Read(out (Vector2 direction, string id)[] bullets, (out (Vector2 direction, string id) t, byte[] buf, int i)=> {
			i = GDEBinaryConverter.Read(out Vector2 v, buf, i);
			i = GDEBinaryConverter.Read(out string idx, buf, i);
			t = (v, idx);
			return i;
		}, buffer, index);

		for (int i = 0; i < bullets.Length; i++)
		{
			MyProjectileData data = new MyProjectileData(ammo, Plane.Identification, MuzzlePosition, Plane.Velocity, bullets[i].direction, GunDamage, bullets[i].id, AmmoScale);
			MyProjectile.AddProjectile(Plane.GetParent(), data);
		}
		_audio.Play();
		if (HideWhenEmpty && AmmoCount == 0)
		{
			Hide();
		}
	}
}

