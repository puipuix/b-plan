using Godot;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//[Tool]
public class MyTurret : MyGun
{
	[Export]
	public float AngleMax { get; set; } = 180;

	[Export]
	public float AngleMin { get; set; } = -180;

	[Export]
	public float RotationSpeed { get; set; } = 75;

	[Export]
	public float RotationSpeedVariation { get; set; } = 15;

	[Export]
	public float AngleToShot { get; set; } = 15;

	[Export]
	public NodePath Gunner { get; set; }
	private MyComponent _gunner;

	private float _angle;
	private float _rotationSpeed;

	public override void _Ready()
	{
		base._Ready();
		_gunner = GetNode<MyComponent>(Gunner);
		_rotationSpeed = RotationSpeed + GDERandom.RNG.NextFloat(-1, 1) * RotationSpeedVariation;
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		_gunner = null;
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (IsNetworkMaster() && Plane.PlayerInput?.PlaneTurretGlobalTarget() is Vector2 target && !_gunner.IsDestroyed)
		{
			_angle = Mathf.Rad2Deg(GlobalTransform.x.AngleTo(target - GlobalPosition)) * (Plane.Reversed ? -1 : 1);
			int dir = Mathf.Clamp((int)_angle, -1, 1);
			float rotation = dir * _rotationSpeed * Effectiveness * delta;
			Rpc(nameof(RPCSetRotation), Mathf.Clamp(RotationDegrees + rotation, AngleMin, AngleMax));
		}
	}

	public override void Shot()
	{
		if (Math.Abs(_angle) < AngleToShot && !_gunner.IsDestroyed)
		{
			base.Shot();
		}
	}

	[PuppetSync]
	private void RPCSetRotation(float r)
	{
		RotationDegrees = r;
	}
}
