using Godot;
using GDExtension;
using System;

//[Tool]
public class MyNameDisplay : Control
{
	private RichTextLabel _label;

	private string _text = "";

	[Export]
	public string Text
	{
		get => _text;
		set {
			_text = value;
			UpdateText();
		}
	}

	private Color _color = Colors.White;

	[Export]
	public Color Color
	{
		get => _color;
		set {
			_color = value;
			UpdateText();
		}
	}

	private float _height;

	[Export]
	public float Height
	{
		get => _height;
		set {
			_height = value;
			UpdateText();
		}
	}

	private void UpdateText()
	{
		if (_label is RichTextLabel l)
		{
			l.BbcodeText = string.Concat("[center][color=#", _color.ToHtml(true), "]", _text, "[/color][/center]");
		}
	}

	public override void _Ready()
	{
		base._Ready();
		_label = GetNodeOrNull<RichTextLabel>("CenterContainer/Name");
		UpdateText();
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_label = null;
			_text = null;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		var parent = GetParentOrNull<Node2D>();
		if (parent != null)
		{
			RectRotation = -parent.GlobalRotationDegrees * parent.Scale.y;
			RectScale = parent.Scale;
		}

		_label.RectPosition = new Vector2(-100, -_height);
	}
}
