using BPlan.Game.Common;
using Godot;
using GDExtension;
using System;

public class MyPlaneDamageStaticBody : MyDamageStaticBody
{
    public MyPlane Plane { get; private set; }

    public override void _Ready()
    {
        base._Ready();
        Plane = this.GetAncestorOrNull<MyPlane>();
    }

    public override void DamageComponent(MyComponent componant)
    {
        if (Plane.Battle.GameRule.IsRamDamageValid(Plane.Identification, componant.Plane.Identification))
        {
            componant.Damage(Damage, EMyLogMessage.Ram, Plane.Identification);
        }
    }
    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            Plane = null;
        }
    }
}
