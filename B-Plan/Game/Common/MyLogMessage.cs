﻿using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;
using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

namespace BPlan.Game.Common
{
    public abstract class MyLogText : IGDEBinaryData, IDisposable
    {
        protected const sbyte StringLogText = 1, PlayerLogText = 2, NillLog = -1;

        internal MyLogText _child;
        internal MyLogText _parent;

        protected MyLogText() { }

        public virtual int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = ReadFromBinaryBuffer(out _child, buffer, index);
            if (_child != null)
            {
                _child._parent = this;
            }
            return index;
        }

        protected MyLogText(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }

        public virtual void Dispose()
        {
            _child?.Dispose();
            _child = null;
        }

        public MyLogText Append(MyLogText text)
        {
            if (text != null)
            {
                if (_child == null)
                {
                    _child = text.Copy();
                    text._parent = this;
                }
                else
                {
                    _child.Append(text);
                }
            }

            return this;
        }

        public static MyLogText operator +(MyLogText a, MyLogText b)
        {
            if (a == null)
            {
                return b;
            }
            else
            {
                return a.Append(b);
            }
        }

        public static implicit operator MyLogText(string a)
        {
            return new MyStringLogtext(a);
        }

        public static implicit operator string(MyLogText a)
        {
            return a.ToString();
        }

        public static implicit operator MyLogText(MyIdentification a)
        {
            return (MyPlayerNameLogText)a;
        }

        public virtual int GetBinaryBufferLength()
        {
            return 1 + (_child?.GetBinaryBufferLength() ?? 1);
        }

        public virtual int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(LogType, buffer, index);
            return WriteToBinaryBuffer(_child, buffer, index);
        }

        public static MyLogText ReadFromBinaryBuffer(byte[] buffer)
        {
            ReadFromBinaryBuffer(out MyLogText log, buffer, 0);
            return log;
        }

        public static int ReadFromBinaryBuffer(out MyLogText text, byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out sbyte type, buffer, index);
            switch (type)
            {
                case NillLog: text = null; break;
                case StringLogText: text = new MyStringLogtext(buffer, ref index); break;
                case PlayerLogText: text = new MyPlayerNameLogText(buffer, ref index); break;
                default: throw new ArgumentException("'" + type + "' isn't a valid log text type id.");
            }
            return index;
        }

        public static int WriteToBinaryBuffer(MyLogText text, byte[] buffer, int index)
        {
            if (text is null)
            {
                return GDEBinaryConverter.Write(NillLog, buffer, index);
            }
            else
            {
                return text.WriteToBinaryBuffer(buffer, index);
            }
        }

        public override string ToString()
        {
            return ToString(new StringBuilder()).ToString();
        }

        public string ToBBCode(MyIdentification? localPlayer = null)
        {
            return ToBBCode(new StringBuilder(), localPlayer).ToString();
        }

        protected StringBuilder AppendColorized(StringBuilder builder, string text, Color color)
        {
            return builder.Append("[color=#").Append(color.ToHtml()).Append("]").Append(text).Append("[/color]");
        }

        public abstract sbyte LogType { get; }
        public abstract MyLogText Copy();
        public abstract StringBuilder ToBBCode(StringBuilder builder, MyIdentification? localPlayer = null);
        public abstract StringBuilder ToString(StringBuilder builder);

        
    }

    public class MyStringLogtext : MyLogText
    {
        public string Value { get; private set; }

        public Color Color { get; private set; }

        public override sbyte LogType => StringLogText;

        public MyStringLogtext(string value, Color? color = null)
        {
            Color = color ?? Colors.White;
            Value = value;
        }

        public override int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = base.ReadFromBinaryBuffer(buffer, index);
            index = GDEBinaryConverter.Read(out string v, buffer, index);
            Value = v;
            index = GDEBinaryConverter.Read(out Color c, buffer, index);
            Color = c;
            return index;
        }

        public MyStringLogtext(byte[] buffer, ref int index) : base(buffer, ref index)
        {

        }

        public override void Dispose()
        {
            base.Dispose();
            Value = null;
        }

        public override StringBuilder ToBBCode(StringBuilder builder, MyIdentification? localPlayer = null)
        {
            AppendColorized(builder, TranslationServer.Translate(Value), Color);
            if (_child != null)
            {
                _child.ToBBCode(builder, localPlayer);
            }
            return builder;
        }
        public override StringBuilder ToString(StringBuilder builder)
        {
            builder.Append(TranslationServer.Translate(Value));
            if (_child != null)
            {
                _child.ToString(builder);
            }
            return builder;
        }

        public override MyLogText Copy()
        {
            return new MyStringLogtext(Value, Color).Append(_child?.Copy()); ;
        }

        public override int GetBinaryBufferLength()
        {
            return base.GetBinaryBufferLength() + GDEBinaryConverter.GetTotalBufferLength(Value, Color);
        }

        public override int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = base.WriteToBinaryBuffer(buffer, index);
            index = GDEBinaryConverter.Write(Value, buffer, index);
            index = GDEBinaryConverter.Write(Color, buffer, index);
            return index;
        }

        public static implicit operator MyStringLogtext(string a)
        {
            return new MyStringLogtext(a);
        }

        public static implicit operator string(MyStringLogtext a)
        {
            return a.ToString();
        }
    }

    public class MyPlayerNameLogText : MyLogText
    {
        public MyIdentification Player { get; private set; }
        public string Name { get; private set; }

        public override sbyte LogType => PlayerLogText;

        public MyPlayerNameLogText(MyIdentification player, string name = null)
        {
            Player = player;
            Name = name ?? Player.GetClient()?.UserName ?? "Error";
        }
        public override int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = base.ReadFromBinaryBuffer(buffer, index);
            index = GDEBinaryConverter.Read(out MyIdentification i, buffer, index);
            Player = i;
            index = GDEBinaryConverter.Read(out string n, buffer, index);
            Name = n;
            return index;
        }

        public MyPlayerNameLogText(byte[] buffer, ref int index) : base(buffer, ref index)
        {
            
        }

        public override void Dispose()
        {
            base.Dispose();
            Name = null;
        }

        public override StringBuilder ToBBCode(StringBuilder builder, MyIdentification? localPlayer = null)
        {
            AppendColorized(builder, Name, MyIdentification.GetIdentificationColor(Player, localPlayer));

            if (_child != null)
            {
                _child.ToBBCode(builder, localPlayer);
            }
            return builder;
        }

        public override StringBuilder ToString(StringBuilder builder)
        {
            builder.Append(Name);
            if (_child != null)
            {
                _child.ToString(builder);
            }
            return builder;
        }

        public override MyLogText Copy()
        {
            return new MyPlayerNameLogText(Player, Name).Append(_child?.Copy()); ;
        }

        public override int GetBinaryBufferLength()
        {
            return base.GetBinaryBufferLength() + GDEBinaryConverter.GetTotalBufferLength(Player, Name);
        }

        public override int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = base.WriteToBinaryBuffer(buffer, index);
            index = GDEBinaryConverter.Write(Player, buffer, index);
            index = GDEBinaryConverter.Write(Name, buffer, index);
            return index;
        }

        public static implicit operator MyPlayerNameLogText(MyIdentification a)
        {
            return new MyPlayerNameLogText(a);
        }
    }

    public static class EMyLogMessage
    {
        public const int Crash = -1, ShotDown = 0, BurnUp = 1, SelfBurn = 9, CauseCrash = 2, Ram = 3, TailOff = 4, Killed = 5, TurnOff = 6, Explosion = 7, SelfExplosion = 8;

        public static readonly string[] DeathMessage = {
            "LOG_WING",
            "LOG_BURN",
            "LOG_CRASH_BY",
            "LOG_RAM",
            "LOG_TAIL",
            "LOG_KILL",
            "LOG_ENGINE",
            "LOG_EXPLOSION",
            "LOG_SELF_EXPLOSION",
            "LOG_SELF_BURN",
        };

        public static MyLogText GetDeathMessage(MyIdentification id1, MyIdentification? id2, int message)
        {
            int index = Crash;
            if (0 <= message && message < DeathMessage.Length)
            {
                index = message;
            }
            if (index == -1)
            {
                return (MyPlayerNameLogText)id1 + string.Concat(" (", EMyPlanes.GetNameFromId(id1.ServerPlaneId), ") ") + "LOG_CRASHED" + ".";
            }
            else if (id2.HasValue)
            {
                return (MyPlayerNameLogText)id1 + string.Concat(" (", EMyPlanes.GetNameFromId(id1.ServerPlaneId), ") ") + DeathMessage[index] + " " + id2.Value + string.Concat(" (", EMyPlanes.GetNameFromId(id2.Value.ServerPlaneId), ").");
            }
            else
            {
                return (MyPlayerNameLogText)id1 + string.Concat(" (", EMyPlanes.GetNameFromId(id1.ServerPlaneId), ") ") + DeathMessage[index] + ".";
            }
        }
    }
}
