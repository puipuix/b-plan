﻿using Godot;
using Godot.Collections;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using GDExtension.Binaries;

namespace BPlan.Game.Common
{
    public struct MyGameRule : IGDEBinaryData
    {
        public bool EnemyFireEnabled { get; private set; }
        public bool SelfFireEnabled { get; private set; }
        public bool FriendlyFireEnabled { get; private set; }
        public bool FriendlyRammingEnabled { get; private set; }
        public bool RammingEnabled { get; private set; }
        
        public MyGameRule(bool enemyFireEnabled, bool selfFireEnabled, bool friendlyFireEnabled, bool friendlyRammingEnabled, bool rammingEnabled)
        {
            EnemyFireEnabled = enemyFireEnabled;
            SelfFireEnabled = selfFireEnabled;
            FriendlyFireEnabled = friendlyFireEnabled;
            FriendlyRammingEnabled = friendlyRammingEnabled;
            RammingEnabled = rammingEnabled;
        }

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out bool b, buffer, index);
            EnemyFireEnabled = b;
            index = GDEBinaryConverter.Read(out b, buffer, index);
            SelfFireEnabled = b;
            index = GDEBinaryConverter.Read(out b, buffer, index);
            FriendlyFireEnabled = b;
            index = GDEBinaryConverter.Read(out b, buffer, index);
            FriendlyRammingEnabled = b;
            index = GDEBinaryConverter.Read(out b, buffer, index);
            RammingEnabled = b;
            return index;
        }

        public MyGameRule(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }
        public MyGameRule(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public bool IsRamDamageValid(MyIdentification a, MyIdentification b)
        {
            return !a.IsSelf(b) && (a.IsFriendly(b) ? FriendlyRammingEnabled : RammingEnabled);
        }

        public bool IsProjectileDamageValid(MyIdentification a, MyIdentification b, bool selfHitProtection)
        {
            return a.IsSelf(b) ? (SelfFireEnabled && !selfHitProtection) : (a.IsFriendly(b) ? FriendlyFireEnabled : EnemyFireEnabled);
        }

        public int GetBinaryBufferLength()
        {
            return 5;
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(EnemyFireEnabled, buffer, index);
            index = GDEBinaryConverter.Write(SelfFireEnabled, buffer, index);
            index = GDEBinaryConverter.Write(FriendlyFireEnabled, buffer, index);
            index = GDEBinaryConverter.Write(FriendlyRammingEnabled, buffer, index);
            index = GDEBinaryConverter.Write(RammingEnabled, buffer, index);
            return index;
        }

        public static readonly MyGameRule Arcade = new MyGameRule(true, false, false, false, false);

        public static readonly MyGameRule Realistic = new MyGameRule(true, false, true, false, true);
 
        public static readonly MyGameRule Simulator = new MyGameRule(true, true, true, true, true);
    }
}
