using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GDExtension;

namespace BPlan.Game.Common
{
	public abstract class MyEntity : KinematicBody2D
	{

		private bool _reversed { get; set; }

		public bool Reversed
		{
			get => _reversed;
			set {
				_reversed = value;
				Scale = new Vector2(1, (_reversed ? -1 : 1));
			}
		}

		private float _rotation;
		public float MyRotation
		{
			get => _rotation;
			set {
				_rotation = value;
				while (_rotation > Mathf.Pi)
				{
					_rotation -= Mathf.Pi * 2;
				}
				while (_rotation < -Mathf.Pi)
				{
					_rotation += Mathf.Pi * 2;
				}
				Rotation = _rotation;
			}
		}

		public Vector2 ForwardVector => Vector2.Right.Rotated(_rotation);
		public Vector2 BackwardVector => Vector2.Left.Rotated(_rotation);
		public Vector2 DownVector => Vector2.Down.Rotated(_rotation);
		public Vector2 UpVector => Vector2.Up.Rotated(_rotation);

		public MyBattle Battle { get; private set; }

		public virtual MyIdentification Identification { get; set; }

		public bool IsRemoteSynchronized { get; protected set; } = true;

		public override void _Ready()
		{
			base._Ready();
			Battle = this.GetAncestorOrNull<MyBattle>();
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			if (disposing)
			{
				Battle = null;
			}
		}

		public void MyRotate(float angle)
		{
			MyRotation += angle;
		}

		public override void _Process(float delta)
		{
			base._Process(delta);
			if (!Engine.EditorHint)
			{
				if (!IsRemoteSynchronized || IsNetworkMaster())
				{
					if (Position.x < -Battle.MapLimit)
					{
						Position = new Vector2(Battle.MapLimit - 10, Position.y);
					}

					if (Battle.MapLimit < Position.x)
					{
						Position = new Vector2(-Battle.MapLimit + 10, Position.y);
					}
				}
				if (IsRemoteSynchronized && IsNetworkMaster())
				{
					Rpc(nameof(RPCSetTransform), Position, MyRotation, Reversed);
				}
			}
		}

		[Puppet]
		private void RPCSetTransform(Vector2 position, float rotation, bool reversed)
		{
			Reversed = reversed;
			Position = position;
			MyRotation = rotation;
		}
	}
}
