using BPlan.Game.Common;
using Godot;
using System;
using System.Net.NetworkInformation;

public class MyDamageStaticBody : StaticBody2D, IMyDamageElement
{
	[Export]
	public int Damage { get; set; } = short.MaxValue;
	public virtual void DamageComponent(MyComponent componant)
	{
		if (componant.CanTakeCollisionDamage)
		{
			componant.Damage(Damage, EMyLogMessage.Crash, null);
		}
	}
}
