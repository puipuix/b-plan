using BPlan.Game.Common.Projectiles;
using Godot;
using GDExtension;
using System;

public class MyBombProjectile : MyProjectile
{
    [Export]
    public float IncendaryDuration { get; set; } = 5;

    [Export]
    public float IncendaryProbability { get; set; } = 1f;

    [Export]
    public int ExplosionDamage { get; set; } = 1000;

    [Export]
    public int ExplosionRadius { get; set; } = 30;

    [Export]
    public int ParticuleCount { get; set; } = 10;

    [Export]
    public int ParticuleRadius { get; set; } = 15;

    [RemoteSync]
    protected override void RPCRemove()
    {
        var expl = MyExplosion.Instance(ExplosionRadius, Identification, Identification, ExplosionDamage, ParticuleCount, ParticuleRadius, IncendaryProbability, IncendaryDuration);
        GetParent().CallDeferred("add_child", expl);
        expl.CallDeferred(expl.Explode, GlobalPosition);
        base.RPCRemove();
    }

    public override void DamageComponent(MyComponent component)
    {
        if (Battle.GameRule.IsProjectileDamageValid(Identification, component.Plane.Identification, SelfHitProtection))
        {
            OnEndOfLife();
        }
    }
}
