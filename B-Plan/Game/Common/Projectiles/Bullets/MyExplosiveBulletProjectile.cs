using Godot;
using System;
using GDExtension;

public class MyExplosiveBulletProjectile : MyBulletProjectile
{
    [Export]
    public int ExplosionDamage { get; set; } = 10;

    [Export]
    public int ExplosionRadius { get; set; } = 10;
    [Export]
    public int ParticuleCount { get; set; } = 3;

    [Export]
    public int ParticuleRadius { get; set; } = 5;

    
    [RemoteSync]
    protected override void RPCRemove()
    {
        var expl = MyExplosion.Instance(ExplosionRadius, Identification, Identification, ExplosionDamage, ParticuleCount, ParticuleRadius, IncendaryProbability, IncendaryDuration);
        GetParent().CallDeferred("add_child", expl);
        expl.CallDeferred(expl.Explode, GlobalPosition);
        base.RPCRemove();
    }
}
