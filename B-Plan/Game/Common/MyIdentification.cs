﻿using BPlan.Game.Common.Projectiles;
using BPlan.Game.Common.Vehicles.Planes;
using BPlan.Game.NetWork;
using Godot;
using Godot.Collections;
using GDExtension.Binaries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public struct MyIdentification : IGDEBinaryData
    {
        public int ServerId { get; private set; }
        public sbyte TeamId { get; private set; }
        public sbyte ServerPlaneId { get; private set; }

        public bool IsValid => ServerId != ErrorId;
        
        public MyIdentification(byte[] buffer, ref int index) : this()
        {
            index = ReadFromBinaryBuffer(buffer, index);
        }

        public MyIdentification(byte[] buffer) : this()
        {
            ReadFromBinaryBuffer(buffer, 0);
        }

        public MyIdentification(int id, sbyte team, sbyte serverPlaneId)
        {
            ServerId = id;
            TeamId = team;
            ServerPlaneId = serverPlaneId;
        }

        public int GetBinaryBufferLength()
        {
            return 6;
        }

        public int WriteToBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Write(ServerId, buffer, index);
            index = GDEBinaryConverter.Write(TeamId, buffer, index);
            index = GDEBinaryConverter.Write(ServerPlaneId, buffer, index);
            return index;
        }

        public int ReadFromBinaryBuffer(byte[] buffer, int index)
        {
            index = GDEBinaryConverter.Read(out int s, buffer, index);
            ServerId = s;
            index = GDEBinaryConverter.Read(out sbyte t, buffer, index);
            TeamId = t;
            index = GDEBinaryConverter.Read(out sbyte p, buffer, index);
            ServerPlaneId = p;
            return index;
        }

        public MyClientData? GetClient()
        {
            return MyBattle.CurrentBattle?.GetClientData(ServerId);
        }

        public bool GetClient(out MyClientData client)
        {
            client = new MyClientData(ErrorId, "Error", EMyPlanes.Biplan, MyAmmoRack.NoneAmmoRacksAllPlane, Colors.Black);
            return MyBattle.CurrentBattle?.GetClientData(ServerId, out client) ?? false;
        }
               
        public override string ToString()
        {
            return string.Concat("{ \"ServerId\": ", ServerId, ", \"TeamId\": ", TeamId, ", \"ServerPlaneId\": ", ServerPlaneId, " }");
        }

        public bool IsFriendly(MyIdentification other)
        {
            return TeamId != ErrorId && TeamId == other.TeamId;
        }

        public bool IsSelf(MyIdentification other)
        {
            return ServerId != ErrorId && ServerId == other.ServerId;
        }

        public override bool Equals(object obj)
        {
            return obj is MyIdentification id && id.ServerId == ServerId;
        }

        public override int GetHashCode()
        {
            return ServerId.GetHashCode();
        }

        public static bool operator ==(MyIdentification a, MyIdentification b)
        {
            return a.ServerId == b.ServerId;
        }

        public static bool operator !=(MyIdentification a, MyIdentification b)
        {
            return a.ServerId != b.ServerId;
        }

        public static readonly sbyte UnknowId = -1;

        public static readonly sbyte ErrorId = -2;

        public static Color GetIdentificationColor(MyIdentification target, MyIdentification? local)
        {
            if (local is null)
            {
                return Colors.Gray;
            }
            else if (local.Value.IsSelf(target))
            {
                return Colors.DarkGreen;
            }
            else if (local.Value.IsFriendly(target))
            {
                return Colors.Blue;
            }
            else
            {
                return Colors.Red;
            }
        }

    }
}
