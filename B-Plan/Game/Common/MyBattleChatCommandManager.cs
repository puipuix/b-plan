﻿using BPlan.Game.Common;
using BPlan.Game.NetWork;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using GDExtension;
using BPlan.Game.Common.PlayerInputs;

namespace BPlan.Game.Common
{
    public static class MyBattleChatCommandManager
    {
        private static MyChatCommandeBase[] _commands = {
            new MyHelpCommand(), new MyClearCommand(),
            new MyStopCommand(), new MyStartCommand(), new MyAutoStartCommand(),
            new MyPingCommand(), new MyRefreshCommand(),
            new MyNextCommand(), new MyRotationCommand(), new MyScenarioCommand(),
            new MyPrintPlayerIdCommand(), new MyKillCommand(), new MyKickCommand(),
            new MyExitCommand(),
            new MyPrintStrayNodesCommand(), new MyPrintTreeCommand(),
            new MyNNClear(), new MyNNLoad(), new MyNNSave(),
        };

        public static void ParseAndExecute(MyBattle battle, string command)
        {
            string[] args = command.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
            var cmds = _commands.Where(cmd => cmd.Name.StartsWith(args[0])).ToArray();
            if (cmds.Length == 1)
            {
                if (cmds[0].IsAviable(battle))
                {
                    cmds[0].Execute(battle, args);
                }
                else
                {
                    Warning(battle, "Unauthorized command.");
                }
            }
            else if (cmds.Length > 1)
            {
                Warning(battle, "Multiple commands possible: ");
                foreach (var item in cmds)
                {
                    battle.LocalLog(new MyStringLogtext(item.GetPattern(), Colors.Yellow));
                }
            }
            else
            {
                Error(battle, "Unknow command.");
            }
        }

        private static void PrintFirstTime(MyBattle battle, string text)
        {
            battle.LocalLog(new MyStringLogtext("> " + text, Colors.Gray));
        }

        private static void Print(MyBattle battle, string text)
        {
            battle.LocalLog(new MyStringLogtext(text, Colors.Gray));
        }

        private static void Error(MyBattle battle, string text)
        {
            battle.LocalLog(new MyStringLogtext("> " + text, Colors.Red));
        }

        private static void Warning(MyBattle battle, string text)
        {
            battle.LocalLog(new MyStringLogtext("> " + text, Colors.Yellow));
        }

        private abstract class MyChatCommandeBase
        {
            protected MyLogText TextForType<T>(string name = null, bool optional = false)
            {
                if (optional)
                {
                    return TextForText(name == null ? "[" : ("[" + name + ":")) + new MyStringLogtext(typeof(T).Name, Colors.Cyan) + TextForText("] ");
                }
                else if (name == null)
                {
                    return new MyStringLogtext(typeof(T).Name + " ", Colors.Cyan);
                }
                else
                {
                    return TextForText("(" + name + ":") + new MyStringLogtext(typeof(T).Name, Colors.Cyan) + TextForText(") ");
                }
            }
            protected MyLogText TextForName() => new MyStringLogtext(Name + " ", Colors.White);
            protected MyLogText TextForText(string text) => new MyStringLogtext(text, Colors.Gray);

            public abstract string Name { get; }
            public abstract bool IsAviable(MyBattle battle);
            public abstract MyLogText GetPattern();
            public abstract MyLogText GetDescription();
            public abstract void Execute(MyBattle battle, string[] args);
        }

        private class MyClearCommand : MyChatCommandeBase
        {
            public override string Name => "/clear";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.ClearChat();
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Clear the chat.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyStartCommand : MyChatCommandeBase
        {
            public override string Name => "/start";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.FightEnabled = true;
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Start the battle.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyRefreshCommand : MyChatCommandeBase
        {
            public override string Name => "/refresh";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.Refresh();
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Refresh the battle datas.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return !battle.IsNetworkMaster();
            }
        }
        private class MyAutoStartCommand : MyChatCommandeBase
        {
            public override string Name => "/auto_start";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length == 1)
                {
                    PrintFirstTime(battle, "Auto start: " + battle.AutoStart);
                }
                else if (args.Length == 2)
                {
                    if ("false".StartsWith(args[1].ToLower()))
                    {
                        battle.AutoStart = false;
                    }
                    else if ("true".StartsWith(args[1].ToLower()))
                    {
                        battle.AutoStart = true;
                    }
                    else
                    {
                        Error(battle, args[1] + " isn't a valid boolean (expected true/false)");
                    }
                }
                else
                {
                    Error(battle, "Expected 0 or 1 argument, got: " + (args.Length - 1) + ".");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Start the battle.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyStopCommand : MyChatCommandeBase
        {
            public override string Name => "/stop";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.FightEnabled = false;
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Start the battle.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyHelpCommand : MyChatCommandeBase
        {
            public override string Name => "/help";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length == 1)
                {
                    PrintFirstTime(battle, "Aviable commands: ");
                    foreach (var item in _commands.Where(cmd => cmd.IsAviable(battle)))
                    {
                        Print(battle, item.GetPattern());
                    }
                }
                else if (args.Length == 2)
                {
                    var input = args[1][0] == '/' ? args[1] : ('/' + args[1]);
                    var cmds = _commands.Where(cmd => cmd.Name.StartsWith(input)).ToArray();
                    if (cmds.Length == 1)
                    {
                        PrintFirstTime(battle, "Command: ");
                        Print(battle, cmds[0].GetPattern());
                        Print(battle, cmds[0].GetDescription());
                    }
                    else if (cmds.Length > 1)
                    {
                        Warning(battle, "Multiple commands possible: ");
                        foreach (var item in cmds)
                        {
                            battle.LocalLog(new MyStringLogtext(item.GetPattern(), Colors.Yellow));
                        }
                    }
                    else
                    {
                        Warning(battle, "Unknow command '" + input + "'.");
                    }
                }
                else
                {
                    Error(battle, "Expected 0 or 1 argument, got: " + (args.Length - 1) + ".");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Print aviable command.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<string>("name", true);
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyPingCommand : MyChatCommandeBase
        {
            public override string Name => "/ping";

            public override void Execute(MyBattle battle, string[] args)
            {
                battle.SendPingRequest();
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Send a ping request.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyNextCommand : MyChatCommandeBase
        {
            public override string Name => "/next";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.AutoLoadScenario();
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Load the next battle scenario.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyRotationCommand : MyChatCommandeBase
        {
            public override string Name => "/rotation_mode";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length == 1)
                {
                    PrintFirstTime(battle, "Current mode: " + battle.ScenarioRotationMode.ToString());
                    Print(battle, "Aviable:");
                    foreach (var item in Enum.GetNames(typeof(EMyScenarioRotationMode)))
                    {
                        Print(battle, item);
                    }
                }
                else if (args.Length == 2)
                {
                    if (battle.IsNetworkMaster())
                    {
                        string[] possible = Enum.GetNames(typeof(EMyScenarioRotationMode)).Where(n => n.StartsWith(args[1])).ToArray();
                        if (possible.Length == 0)
                        {
                            Warning(battle, "Unknow mode: '" + args[1] + "'.");
                        }
                        else if (possible.Length > 1)
                        {
                            Warning(battle, "Multiple values possible:");
                            foreach (var item in possible)
                            {
                                battle.LocalLog(new MyStringLogtext(item, Colors.Yellow));
                            }
                        }
                        else
                        {
                            battle.ScenarioRotationMode = (EMyScenarioRotationMode)Enum.Parse(typeof(EMyScenarioRotationMode), possible[0]);
                            PrintFirstTime(battle, "Mode set to: " + battle.ScenarioRotationMode.ToString());
                        }
                    }
                    else
                    {
                        Warning(battle, "Unauthorized command.");
                    }
                }
                else
                {
                    Error(battle, "Expected 0 or 1 argument, got: " + (args.Length - 1) + ".");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Get or set the scenario rotation mode.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<string>("name", true);
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyScenarioCommand : MyChatCommandeBase
        {
            public override string Name => "/scenario";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length == 1)
                {
                    PrintFirstTime(battle, "Current scenario list: ");
                    for (int i = 0; i < battle.ScenarioRotation.Count; i++)
                    {
                        Print(battle, battle.ScenarioRotation[i].Name + "(index: " + i + ")");
                    }
                }
                else if (args.Length == 2 || args.Length == 3 || args.Length == 4)
                {
                    if (battle.IsNetworkMaster())
                    {
                        if ("select".StartsWith(args[1]))
                        {
                            if (args.Length != 3)
                            {
                                Error(battle, "Expected 1 argument for select operation, got: " + (args.Length - 2) + ".");
                            }
                            else
                            {
                                if (int.TryParse(args[2], out int index))
                                {
                                    if (index >= 0 && index < battle.ScenarioRotation.Count)
                                    {
                                        battle.LoadScenario(index);
                                    }
                                    else
                                    {
                                        Warning(battle, "Index invalid: '" + index + "' < 0 or " + (battle.ScenarioRotation.Count - 1) + " < '" + index + "'.");
                                    }
                                }
                                else
                                {
                                    Error(battle, "'" + args[2] + "' isn't a valid integer.");
                                }
                            }
                        }
                        else if ("aviable".StartsWith(args[1]))
                        {
                            if (args.Length != 2)
                            {
                                Error(battle, "Expected 0 argument for aviable operation, got: " + (args.Length - 2) + ".");
                            }
                            else
                            {
                                PrintFirstTime(battle, "Possible scenarios:");
                                foreach (var item in EMyScenario.GetScenarios())
                                {
                                    Print(battle, item.Name);
                                }
                            }
                        }
                        else if ("add".StartsWith(args[1]))
                        {
                            MyScenario[] possible = EMyScenario.GetScenarios(args[2]);
                            if (possible.Length == 0)
                            {
                                Warning(battle, "Unknow scenario: '" + args[2] + "'.");
                            }
                            else if (possible.Length > 1)
                            {
                                Warning(battle, "Multiple values possible:");
                                foreach (var item in possible)
                                {
                                    battle.LocalLog(new MyStringLogtext(item.Name, Colors.Yellow));
                                }
                            }
                            else
                            {
                                if (args.Length == 3)
                                {
                                    battle.ScenarioRotation.Add(possible[0]);
                                    PrintFirstTime(battle, "'" + possible[0].Name + "' scenario added.");
                                }
                                else if (int.TryParse(args[3], out int index))
                                {
                                    battle.ScenarioRotation.Insert(index, possible[0]);
                                    PrintFirstTime(battle, "'" + possible[0].Name + "' scenario inserted in position " + index + ".");
                                }
                                else
                                {
                                    Error(battle, "'" + args[3] + "' isn't a valid integer.");
                                }
                            }
                        }
                        else if ("remove".StartsWith(args[1]))
                        {
                            if (args.Length != 3)
                            {
                                Error(battle, "Expected 1 arguments for remove operation, got: " + (args.Length - 2) + ".");
                            }
                            else
                            {
                                if (int.TryParse(args[2], out int index))
                                {
                                    if (battle.ScenarioRotation.Count > 1)
                                    {
                                        battle.ScenarioRotation.RemoveAt(index);
                                        Print(battle, "Done.");
                                    }
                                    else
                                    {
                                        Warning(battle, "Invalid operation: The list can't be empty.");
                                    }
                                }
                                else
                                {
                                    Error(battle, "'" + args[2] + "' isn't a valid integer.");
                                }
                            }
                        }
                        else
                        {
                            Error(battle, "Unknow operation '" + args[1] + "' (expected 'add' or 'remove').");
                        }
                    }
                    else
                    {
                        Warning(battle, "Unauthorized command.");
                    }
                }
                else
                {
                    Error(battle, "Expected 0, 2 or 3 argument(s), got: " + (args.Length - 1) + ".");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Print or change the scenario rotation list.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForText("[(add ") + TextForType<string>("name") + TextForType<int>("index", true) + TextForText("| remove ") + TextForType<int>("index") + TextForText("| select ") + TextForType<int>("index") + TextForText(")]");
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyKillCommand : MyChatCommandeBase
        {
            public override string Name => "/kill";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 2)
                {
                    Error(battle, "Expected 1 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    if (int.TryParse(args[1], out int id))
                    {
                        MyPlane plane = battle.GetPlanes().Where(p => p.Identification.ServerId == id).FirstOrDefault();
                        if (plane != null)
                        {
                            plane.Kill();
                        }
                        else
                        {
                            Warning(battle, "Plane not found.");
                        }
                    }
                    else
                    {
                        Error(battle, "'" + args[1] + "' isn't a valid integer.");
                    }
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Kill the plane from his id.");
            }


            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<int>();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyPrintPlayerIdCommand : MyChatCommandeBase
        {
            public override string Name => "/print_player_id";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length < 1 || 3 < args.Length)
                {
                    Error(battle, "Expected 0, 1 or 2 argument(s), got: " + (args.Length - 1) + ".");
                }
                else
                {
                    int team = -1;
                    string name = null;
                    if (args.Length >= 2)
                    {
                        if (!int.TryParse(args[1], out team))
                        {
                            Error(battle, "'" + args[1] + "' isn't a valid integer.");
                            return;
                        }
                    }
                    if (args.Length == 3)
                    {
                        name = args[2];
                    }
                    PrintFirstTime(battle, "Player(s) id: ");
                    foreach (var plane in battle.GetPlanes(team).Where(p => string.IsNullOrWhiteSpace(name) || (p.Identification.GetClient()?.UserName ?? "Error").StartsWith(name)))
                    {
                        Print(battle, (plane.Identification.GetClient()?.UserName ?? "Error") + "(team: " + plane.Identification.TeamId + "): " + plane.Identification.ServerId);
                    }
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Print players ids.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForText("[team: ") + TextForType<int>() + TextForText("[name: ") + TextForType<string>() + TextForText("]]");
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyExitCommand : MyChatCommandeBase
        {
            public override string Name => "/exit";

            public override void Execute(MyBattle battle, string[] args)
            {
                MyNetworkManager.CurrentInstance.CallDeferred(MyNetworkManager.CurrentInstance.StopNetworking, "BATTLE_LEAVED");
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Exit the battle and close/leave the server.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyKickCommand : MyChatCommandeBase
        {
            public override string Name => "/kick";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 2)
                {
                    Error(battle, "Expected 1 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    if (int.TryParse(args[1], out int id))
                    {
                        if (MyNetworkManager.CurrentInstance.PlayersData.TryGetValue(id, out var _))
                        {
                            MyNetworkManager.CurrentInstance.Peer.DisconnectPeer(id);
                        }
                        else
                        {
                            Warning(battle, "Player not found.");
                        }
                    }
                    else
                    {
                        Error(battle, "'" + args[1] + "' isn't a valid integer.");
                    }
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Kick a player from his id.");
            }


            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<int>();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return battle.IsNetworkMaster();
            }
        }
        private class MyPrintStrayNodesCommand : MyChatCommandeBase
        {
            public override string Name => "/print_stray_nodes";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    battle.PrintStrayNodes();
                    PrintFirstTime(battle, "The result as been printed to console.");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Ouput to console the name of all orphan nodes.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyPrintTreeCommand : MyChatCommandeBase
        {
            public override string Name => "/print_tree";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length > 2)
                {
                    Error(battle, "Expected 0 or 1 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    bool value = false;
                    bool err = false;
                    if (args.Length == 2)
                    {
                        if ("false".StartsWith(args[1].ToLower()))
                        {
                            value = false;
                        }
                        else if ("true".StartsWith(args[1].ToLower()))
                        {
                            value = true;
                        }
                        else
                        {
                            value = false;
                            err = true;
                            Error(battle, args[1] + " isn't a valid boolean. (expected true/false)");
                        }
                    }
                    if (!err)
                    {
                        if (value)
                        {
                            battle.PrintTreePretty();
                        }
                        else
                        {
                            battle.PrintTree();
                        }
                        PrintFirstTime(battle, "The result as been printed to console.");
                    }
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Ouput to console the current scene tree.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<bool>("pretty", true);
            }

            public override bool IsAviable(MyBattle battle)
            {
                return true;
            }
        }
        private class MyNNClear : MyChatCommandeBase
        {
            public override string Name => "/nclear";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 1)
                {
                    Error(battle, "Expected 0 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    MyNeuralHelper.ClearNetworks();
                    Print(battle, "Cleared.");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Clear all neural network data.");
            }

            public override MyLogText GetPattern()
            {
                return TextForName();
            }

            public override bool IsAviable(MyBattle battle)
            {
                return MyNeuralHelper.NetworkComputer.IsAccessible();
            }
        }
        private class MyNNLoad : MyChatCommandeBase
        {
            public override string Name => "/nload";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 2)
                {
                    Error(battle, "Expected 1 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    MyNeuralHelper.Load(args[1]);
                    Print(battle, "Loaded.");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Load a network from file");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<string>("path");
            }

            public override bool IsAviable(MyBattle battle)
            {
                return MyNeuralHelper.NetworkComputer.IsAccessible();
            }
        }
        private class MyNNSave : MyChatCommandeBase
        {
            public override string Name => "/nsave";

            public override void Execute(MyBattle battle, string[] args)
            {
                if (args.Length != 2)
                {
                    Error(battle, "Expected 1 argument, got: " + (args.Length - 1) + ".");
                }
                else
                {
                    MyNeuralHelper.Save(args[1]);
                    Print(battle, "Saved.");
                }
            }

            public override MyLogText GetDescription()
            {
                return TextForText("Save the best network to file");
            }

            public override MyLogText GetPattern()
            {
                return TextForName() + TextForType<string>("path");
            }

            public override bool IsAviable(MyBattle battle)
            {
                return MyNeuralHelper.NetworkComputer.IsAccessible();
            }
        }
    }
}
