using BPlan.Game.Menu;
using Godot;
using GDExtension;
using System;

public class MyPerformanceDisplay : VBoxContainer
{

	[GDEReady("Fps", EGDEResearchMode.Descendant)]
	private Label _fps = null;

	[GDEReady("Obj", EGDEResearchMode.Descendant)]
	private Label _obj = null;

	public override void _Ready()
	{
		base._Ready();
		GDEReadyAttribute.SetUp(this);
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_obj = null;
			_fps = null;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		if (MyGameSettings.ShowDebug)
		{
			Visible = true;
			_fps.Text = (int)Performance.GetMonitor(Performance.Monitor.TimeFps)
		   + " (" + (int)(Performance.GetMonitor(Performance.Monitor.TimeProcess) * 1000)
		   + "/" + (int)(Performance.GetMonitor(Performance.Monitor.TimePhysicsProcess) * 1000) + ")";
			_obj.Text = (int)Performance.GetMonitor(Performance.Monitor.ObjectCount)
				+ "/" + (int)Performance.GetMonitor(Performance.Monitor.ObjectNodeCount)
				+ "/" + (int)Performance.GetMonitor(Performance.Monitor.ObjectOrphanNodeCount);
		} else
		{
			Visible = false;
		}
	}
}
