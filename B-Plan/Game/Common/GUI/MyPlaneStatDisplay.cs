using BPlan.Game;
using BPlan.Game.Common.Vehicles.Planes;
using Godot;
using GDExtension;
using System;
using System.Linq;

public class MyPlaneStatDisplay : VBoxContainer
{

    [GDEReady("PlaneName", EGDEResearchMode.Descendant)]
    private Label _name = null;

    [GDEReady("Speed", EGDEResearchMode.Descendant)]
    private Label _speed = null;

    [GDEReady("Altitude", EGDEResearchMode.Descendant)]
    private Label _altitude = null;

    [GDEReady("Thust", EGDEResearchMode.Descendant)]
    private Label _thrust = null;

    [GDEReady("Stalling", EGDEResearchMode.Descendant)]
    private Label _stalling = null;

    [GDEReady("Debug", EGDEResearchMode.Descendant)]
    private Label _debugLabel = null;

    [GDEReady("GunInfos", EGDEResearchMode.Descendant)]
    private VBoxContainer _gunInfos = null;

    [GDEReady("DebugContainer", EGDEResearchMode.Descendant)]

#pragma warning disable CS0414, IDE0051 // not used
    private Container _debugContainer = null;
#pragma warning restore CS0414, IDE0051

    private MyPlane _plane;
    public MyPlane Plane
    {
        get => _plane;
        set {
            _plane = value;
            SetUp();
        }
    }

    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            _name = _speed = _altitude = _thrust = _stalling = null;
            _gunInfos = null;
            _plane = null;
        }
    }

    private void SetUp()
    {
        _name.Text = _plane == null ? "-" : EMyPlanes.GetNameFromId(_plane.Identification.ServerPlaneId);
        _gunInfos.RemoveAllChildren(n => n.QueueFree());
        if (_plane.IsAccessible())
        {
            foreach (var gun in _plane.GetChildren().OfType<MyGun>())
            {
                MyGunStatDisplay ds = MyGunStatDisplay.Instance();
                _gunInfos.AddChild(ds);
                ds.Gun = gun;
            }
        }

    }

    public override void _Process(float delta)
    {
        base._Process(delta);

        if (_plane != null && IsInstanceValid(_plane))
        {
            _altitude.Text = (int)MyConst.PxToAltitude(_plane.Position.y) + "m";
            _speed.Text = (int)MyConst.PxsToKmh(_plane.Velocity.Length()) + "km/h";
            _thrust.Text = Mathf.RoundToInt(_plane.EngineThrottle * 100) + "%";

            _stalling.Text = _plane.IsStalling.ToString();
            _debugLabel.Text = _plane.DebugInfo;
            _debugContainer.Show();
        }
        else
        {
            _plane = null;
            _altitude.Text = "-";
            _speed.Text = "-";
            _thrust.Text = "0%";
            _stalling.Text = "False";
        }
    }
}
