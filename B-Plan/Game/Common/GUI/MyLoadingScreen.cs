using Godot;
using GDExtension;
using System;

public class MyLoadingScreen : CanvasLayer
{
    private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyLoadingScreen.tscn");

    public static MyLoadingScreen Instance()
    {
        return (MyLoadingScreen)_packedScene.Instance();
    }


    [GDEReady("Progress", EGDEResearchMode.Descendant)]
    private ProgressBar _progress = null;
    [GDEReady("Text", EGDEResearchMode.Descendant)]
    private Label _text = null;
    [GDEReady("BackGround")]
    private Control _bg = null;


    private string _txt = "LOAD_LOADING";
    public string Text
    {
        get => _txt;
        set {
            _txt = value;
            if (_text.IsAccessible())
            {
                _text.Text = _txt;
            }
        }
    }

    private GDEDeferredCalls _call = null;

    public float Progress
    {
        get => (float)_progress.Value * 0.01f;
        set => _progress.Value = value * 100;
    }

    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
        _text.Text = _txt;
    }

    public void FadeOut()
    {
        if (_bg.Visible)
        {
            _call = new GDEDeferredCalls(GetTree(), 0.01f)
           .PushFor(0, i => i < 100, i => i + 3, i => _bg.Modulate = new Color(1, 1, 1, 1 - i * 0.01f))
           .PushSingle(() => _bg.Visible = false);
            _call.Start();
        }       
    }

    public void PopUp()
    {
        _bg.Modulate = new Color(1, 1, 1, 1);
        _bg.Visible = true;
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (_call.IsAccessible())
        {
            _call?.Stop();
            _call?.CallDeferred("free");
        }
    }
}
