using BPlan.Game.Common;
using BPlan.Game.NetWork;
using Godot;
using GDExtension;
using System;
using System.Linq;

public class MyBattleMenu : TabContainer
{
    #region Nodes
    [GDEReady("", EGDEResearchMode.Ancestor)]
    private MyBattle _battle = null;

    [GDEReady("Exit", EGDEResearchMode.Descendant)]
    private Button _exit = null;

    [GDEReady("Close", EGDEResearchMode.Descendant)]
    private Button _close = null;

    [GDEReady("Pause", EGDEResearchMode.Descendant)]
    private Button _pause = null;

    [GDEReady("Resume", EGDEResearchMode.Descendant)]
    private Button _resume = null;

    [GDEReady("SpectateOn", EGDEResearchMode.Descendant)]
    private Button _spectate = null;

    [GDEReady("SpectateOff", EGDEResearchMode.Descendant)]
    private Button _spectateOff = null;

    [GDEReady("AddContainer", EGDEResearchMode.Descendant)]
    private VBoxContainer _addContainer = null;

    [GDEReady("RemoveContainer", EGDEResearchMode.Descendant)]
    private VBoxContainer _removeContainer = null;

    [GDEReady("Next", EGDEResearchMode.Descendant)]
    private Button _next = null;

    [GDEReady("RotationMode", EGDEResearchMode.Descendant)]
    private OptionButton _rotationMode = null;

    [GDEReady("IACount", EGDEResearchMode.Descendant)]
    private SpinBox _iACount = null;

    [GDEReady("Players", EGDEResearchMode.Descendant)]
    private VBoxContainer _players = null;

    [GDEReady("NNVisual", EGDEResearchMode.Descendant)]
    private MarginContainer _nnvisual = null;

    #endregion


    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
        _exit.Connect("pressed", this, Exit);
        _close.Connect("pressed", this, MyHide);
        _pause.Connect<bool>("pressed", this, SetFightEnabled, false);
        _resume.Connect<bool>("pressed", this, SetFightEnabled, true);
        _next.Connect("pressed", _battle, _battle.AutoLoadScenario);
        _spectate.Connect("pressed", this, SpectateOn);
        _spectateOff.Connect("pressed", this, SpectateOff);

        _pause.Hide();
        _resume.Hide();
        _spectateOff.Hide();


        foreach (var item in Enum.GetNames(typeof(EMyScenarioRotationMode)))
        {
            _rotationMode.AddItem(item);
        }
        _rotationMode.Select((int)_battle.ScenarioRotationMode);
        _rotationMode.Connect<int>("item_selected", this, OnRotationSelected);

        _iACount.Value = _battle.IACount;
        _iACount.Connect<float>("value_changed", this, IACountChanged);

        SetTabDisabled(2, true);
        SetNNVisualVisible(false);
    }

    public void SetNNVisual(MyNeuralNetworkComputer computer)
    {
        _nnvisual.RemoveAllChildren();
        _nnvisual.AddChild(computer);
    }

    public void SetUpNetWork()
    {
        SetTabDisabled(2, !_battle.IsNetworkMaster());
        GetTree().Connect("network_peer_connected", this, nameof(OnPlayerConnected));
        GetTree().Connect("network_peer_disconnected", this, nameof(OnPlayerDisconnected));
        foreach (var item in EMyScenario.GetScenarios())
        {
            _addContainer.AddChild(MyScenarioDisplay.InstanceForAdd(item, _removeContainer));
        }
        for (int i = 0; i < _battle.ScenarioRotation.Count; i++)
        {
            _removeContainer.AddChild(MyScenarioDisplay.InstanceForRemove(_battle.ScenarioRotation[i], i));
        }
    }

    public void SetNNVisualVisible(bool visible)
    {
        SetTabDisabled(3, !visible);
    }

    private void OnPlayerConnected(int id)
    {
        this.CallDeferred(AddClientFromId, id);
    }

    private void AddClientFromId(int networkId)
    {
        if (MyNetworkManager.CurrentInstance.PlayersData.TryGetValue(networkId, out var data))
        {
            AddClient(data);
        }
        else
        {
            GDEDebug.PrintErr("Unknow client id: " + networkId);
        }
    }

    private void IACountChanged(float value)
    {
        _battle.IACount = Math.Max(1, Mathf.RoundToInt(value));
    }

    private void OnPlayerDisconnected(int networkId)
    {
        var ch = _players.GetChildren().OfType<MyPlayerDisplay>().Where(p => p.Id == networkId).FirstOrDefault();
        if (ch != null)
        {
            _players.RemoveChild(ch);
            ch.QueueFree();
        }
        else
        {
            GDEDebug.PrintErr("Unknow client id: " + networkId);
        }
    }

    private void AddClient(MyClientData data)
    {
        if (!data.IsNPC)
        {
            var p = MyPlayerDisplay.Instance(data.UserName, data.NetworkId, _battle.IsNetworkMaster());
            _players.AddChild(p);
        }
    }

    private void OnRotationSelected(int index)
    {
        _battle.ScenarioRotationMode = (EMyScenarioRotationMode)index;
    }

    private void SetFightEnabled(bool value)
    {
        if (_battle.IsNetworkMaster())
        {
            _battle.FightEnabled = value;
        }
    }

    private void MyHide()
    {
        Hide();
    }

    private void Exit()
    {
        MyNetworkManager.CurrentInstance.CallDeferred(MyNetworkManager.CurrentInstance.StopNetworking, "BATTLE_LEAVED");
    }

    private void SpectateOn()
    {
        _spectate.Hide();
        _spectateOff.Show();
        _battle.SpectateOn();
    }

    private void SpectateOff()
    {
        _spectateOff.Hide();
        _spectate.Show();
        _battle.SpectateOff();
    }


    public override void _Process(float delta)
    {
        base._Process(delta);
        if (_battle.NetWorkEstablished && _battle.IsNetworkMaster())
        {
            if (_resume.Visible != !_battle.FightEnabled)
            {
                _resume.Visible = !_battle.FightEnabled;
            }

            if (_pause.Visible != _battle.FightEnabled)
            {
                _pause.Visible = _battle.FightEnabled;
            }
        }
    }

}
