using BPlan.Game.NetWork;
using Godot;
using GDExtension;
using System;

public class MyPlayerDisplay : HBoxContainer
{
	private static readonly PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyPlayerDisplay.tscn");

	public static MyPlayerDisplay Instance(string name, int id, bool kick)
	{
		var p = (MyPlayerDisplay)_packedScene.Instance();
		p._name = name;
		p.Id = id;
		p._kick = kick;
		return p;
	}

	[GDEReady("Name")]
	private Label _nameDisplay = null;

	[GDEReady("Kick")]
	private TextureButton _kickButton = null;

	public int Id { get; private set; }
	private string _name;
	private bool _kick;

	public override void _Ready()
	{
		base._Ready();
		GDEReadyAttribute.SetUp(this);
		_nameDisplay.Text = _name;
		_kickButton.Visible = _kick && Id != 1;
		_kickButton.Connect("pressed", this, Kick);
	}

	private void Kick()
	{
		var net = MyNetworkManager.CurrentInstance;
		if (net.Battle.IsNetworkMaster())
		{
			net.Peer.DisconnectPeer(Id);
		}
	}
}
