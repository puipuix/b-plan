using Godot;
using GDExtension;
using System;

public class MyTouchScreen : Control
{

    private const double ClickTime = 0.25;
    private const double DoubleClickTime = 0.75;
    private const float DragDeadZone = 50;
    private const float DragTurnDistance = 200;

    [Export]
    public bool ForceVisible { get; set; } = false;

    public bool TurretPressed { get; private set; }
    
    public bool GunPressed { get; private set; }

    public bool IncreaseThrustPressed { get; private set; }
    public bool DecreaseThrustPressed { get; private set; }

    public float TurnPressed { get; private set; }
    public float TurnInvPressed { get; private set; }

    public bool Enabled => OS.HasTouchscreenUiHint();

    public Vector2? TurretDirection { get; private set; }

    [GDEReady("Left")]
    private TextureRect _leftDisplay = null;
    [GDEReady("Right")]
    private TextureRect _RightDisplay = null;
    private Vector2 _lastLeftTouchPosition = Vector2.Zero;
    private Vector2 _lastRightTouchPosition = Vector2.Zero;
    private DateTime _lastLeftTouchDate = DateTime.MinValue;
    private DateTime _lastRightTouchDate = DateTime.MinValue;
    private bool _lastLeftTouchPressed = false;
    private bool _lastRightTouchPressed = false;
    private DateTime _lastLeftClick = DateTime.MinValue;
    private DateTime _lastRightClick = DateTime.MinValue;

    private bool _reverseJustPressed;
    private bool _showDrag => ForceVisible || OS.HasTouchscreenUiHint();

    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
        GetNode<Control>("VSeparator").Visible = _showDrag;
    }

    public bool FlushReverseJustPressed()
    {
        var tmp = _reverseJustPressed;
        _reverseJustPressed = false;
        return tmp;
    }

    private void Replace(TextureRect rect, Vector2 position)
    {
        rect.RectPosition = position - rect.RectSize / 2;
    }
    
    public override void _Input(InputEvent ev)
    {
        if (Enabled && ev is InputEventScreenDrag drag)
        {
            if (drag.Position.x < GetViewportRect().Size.x / 2)
            {
                // LEFT
                if (_lastLeftTouchPressed)
                {
                    _leftDisplay.Visible = true;
                    var relative = drag.Position - _lastLeftTouchPosition;
                    if (relative.x < -DragDeadZone)
                    {
                        TurnPressed = Math.Min(1, (-relative.x - DragDeadZone) / DragTurnDistance);
                    }
                    else
                    {
                        TurnPressed = 0;
                    }

                    if (relative.x > DragDeadZone)
                    {
                        TurnInvPressed = Math.Min(1, (relative.x - DragDeadZone) / DragTurnDistance);
                    }
                    else
                    {
                        TurnInvPressed = 0;
                    }

                    if (relative.y < -DragDeadZone)
                    {
                        IncreaseThrustPressed = true;
                    }
                    else
                    {
                        IncreaseThrustPressed = false;
                    }

                    if (relative.y > DragDeadZone)
                    {
                        DecreaseThrustPressed = true;
                    }
                    else
                    {
                        DecreaseThrustPressed = false;
                    }
                }
            } else
            {
                //RIGHT
                if (_lastRightTouchPressed)
                {
                    _RightDisplay.Visible = true;
                    var relative = drag.Position - _lastRightTouchPosition;
                    if (relative.LengthSquared() > DragDeadZone * DragDeadZone)
                    {
                        TurretDirection = relative.Normalized();
                        TurretPressed = true;
                    } else
                    {
                        TurretDirection = null;
                        TurretPressed = false;
                    }
                }
            }
        }
        if (Enabled && ev is InputEventScreenTouch touch)
        {
            if (touch.Position.x < GetViewportRect().Size.x / 2)
            {
                // LEFT
                if (touch.Pressed)
                {
                    Replace(_leftDisplay, touch.Position);
                }
                else
                {
                    IncreaseThrustPressed = false;
                    DecreaseThrustPressed = false;
                    TurnInvPressed = 0;
                    TurnPressed = 0;
                    _leftDisplay.Visible = false;

                    if ((DateTime.Now - _lastLeftTouchDate).TotalSeconds < ClickTime)
                    {
                        _reverseJustPressed = true;
                        _lastLeftClick = DateTime.Now;
                    }
                }

                _lastLeftTouchDate = DateTime.Now;
                _lastLeftTouchPressed = touch.Pressed;
                _lastLeftTouchPosition = touch.Position;
            }
            else
            {
                // RIGHT
                if (touch.Pressed)
                {
                    Replace(_RightDisplay, touch.Position);
                    if ((DateTime.Now - _lastRightClick).TotalSeconds < DoubleClickTime)
                    {
                        GunPressed = true;
                        _lastRightClick = DateTime.MinValue;
                    }
                }
                else
                {
                    TurretDirection = null;
                    TurretPressed = false;
                    GunPressed = false;
                    _RightDisplay.Visible = false;

                    if ((DateTime.Now - _lastRightTouchDate).TotalSeconds < ClickTime)
                    {
                        _lastRightClick = DateTime.Now;
                    }
                }
                _lastRightTouchDate = DateTime.Now;
                _lastRightTouchPressed = touch.Pressed;
                _lastRightTouchPosition = touch.Position;
            }
        }
        base._Input(ev);
    }
}
