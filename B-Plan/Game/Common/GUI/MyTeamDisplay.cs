using BPlan.Game.Common.Vehicles.Planes;
using Godot;
using GDExtension;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class MyTeamDisplay : VBoxContainer
{
	private static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyTeamDisplay.tscn");

	public static MyTeamDisplay Instance()
	{
		return (MyTeamDisplay)_packedScene.Instance();
	}

	public Label Team { get; private set; }
	public VBoxContainer List { get; private set; }

	private Dictionary<MyPlane, Label> _labels = new Dictionary<MyPlane, Label>();

	public override void _Ready()
	{
		base._Ready();

		Team = GetNode<Label>("Team");
		List = GetNode<VBoxContainer>("List");
	}
	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			Team = null;
			List = null;
			_labels.Clear();
			_labels = null;
		}
	}

	private string GetText(MyPlane plane)
	{
		return (plane.Identification.GetClient()?.UserName ?? "Error") + " (" + EMyPlanes.GetNameFromId(plane.PlaneId) + "): " + plane.KillCount;
	}

	public void SetPlayers(MyBattle battle, int team)
	{
		List.RemoveAllChildren(c => c.QueueFree());
		Team.Text = (battle.ScenarioData?.TeamNames[team] ?? (Tr("TD_TEAM") + " "+ (team + 1))) + " (id: " + team + ")";

		_labels.Clear();
		foreach (var item in battle.GetPlanes(team))
		{
			var lab = new Label() { Text = GetText(item) };
			lab.AddColorOverride("font_color", item.IsAlive ? Colors.White : Colors.Gray);
			List.AddChild(lab);
			_labels.Add(item, lab);
		}
	}

	public bool UpdatePlayer(MyPlane plane)
	{
		if (_labels.TryGetValue(plane, out Label lab))
		{
			lab.Text = GetText(plane);
			lab.AddColorOverride("font_color", plane.IsAlive ? Colors.White : Colors.Gray);
			return true;
		}
		else
		{
			return false;
		}
	}
}
