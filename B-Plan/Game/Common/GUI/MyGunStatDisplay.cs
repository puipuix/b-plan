using Godot;
using GDExtension;
using System;

public class MyGunStatDisplay : HBoxContainer
{
	private readonly static PackedScene _packedScene = GD.Load<PackedScene>("res://Game/Common/GUI/MyGunStatDisplay.tscn");
	public static MyGunStatDisplay Instance() => (MyGunStatDisplay)_packedScene.Instance();

	[GDEReady("Name")]
	private Label _name = null;
	[GDEReady("Ammo")]
	private Label _ammo = null;

	private MyGun _gun;
	public MyGun Gun {
		get => _gun;
		set {
			_gun = value;
			_name.Text = Tr(value?.Name ?? "Gun") + ":";
		}
	}

	public override void _Ready()
	{
		base._Ready();
		GDEReadyAttribute.SetUp(this);
	}

	protected override void Dispose(bool disposing)
	{
		base.Dispose(disposing);
		if (disposing)
		{
			_name = null;
			_gun = null;
			_ammo = null;
		}
	}

	public override void _Process(float delta)
	{
		base._Process(delta);
		_ammo.Text = (_gun?.AmmoCount ?? 0) + "/" + (_gun?.MaxAmmoCount ?? 0);
	}
}
