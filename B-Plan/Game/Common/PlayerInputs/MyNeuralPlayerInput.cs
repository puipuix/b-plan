﻿using GDExtension;
using Godot;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BPlan.Game.Common.PlayerInputs
{
    public class MyNeuralPlayerInput : MyPlayerInput
    {
        public const float ReverseCoolDown = 0.25f;
        public const int DetectZoneCount = 6;
        public const float ActivationValue = 0.5f;
        public const float DoubleActivationValue = 0.25f;

        private static readonly PackedScene _scene = GD.Load<PackedScene>("res://Game/Common/PlayerInputs/MyNeuralPlayerInput.tscn");
        public static MyNeuralPlayerInput Instance()
        {
            var neu = (MyNeuralPlayerInput)_scene.Instance();
            return neu;
        }

        [GDEReady("MyDetectionZoneF")]
        private MyDetectionZone forwardZone = null;
        [GDEReady("MyDetectionZoneFU1")]
        private MyDetectionZone forwardUpZone = null;
        [GDEReady("MyDetectionZoneFD1")]
        private MyDetectionZone forwardDownZone = null;

        public override EMyPlayerInputType Type => EMyPlayerInputType.Neural;

        private MyNeuralPlayerInput() : base() { }

        #region NeuralOutputValue

        private float _throttle;
        private float _gun1;
        private float _gun2;
        private float _gun3;
        private float _gun4;
        private float _rotate;
        private float _reverse;
        #endregion

        private int _network;
        private float _reverseCD = 0;
        private MyDetectionZone[] _zones;
        private int _skipRead = 3;


        #region Fitness
        private float _oldX;

        private double _fitdistance;
        private double _fitAlt;
        private double _fitKill;
        private double _fitAmmo;
        private double _fitTime;
        private double _fitPassive;
        private double _fitDamage;
        private double _fitFolow;
        private double _fitDead;

        private double Fitness => _fitDead + _fitFolow + _fitTime + _fitdistance + +_fitDamage + _fitPassive + _fitAlt + _fitAmmo + _fitKill;
        #endregion

        public override void _Ready()
        {
            base._Ready();
            GDEReadyAttribute.SetUp(this);
            if (IsNetworkMaster())
            {
                _zones = GetChildren().OfType<MyDetectionZone>().ToArray();
                if (_zones.Length != DetectZoneCount)
                {
                    throw new ArgumentException(_zones.Length + "!=" + DetectZoneCount);
                }
            }

        }

        public override void Initialize(MyPlane plane)
        {
            base.Initialize(plane);
            if (IsNetworkMaster())
            {
                _skipRead = 60 * 1; // wait x second
                _oldX = plane.Position.x;
                _network = MyNeuralHelper.GetForId(plane.Identification.ServerId).network;
                GDEDebug.Print("AI_" + plane.Identification.ServerId + " linked with Network_" + _network);
                for (int i = 0; i < _zones.Length; i++)
                {
                    _zones[i].Plane = plane;
                }
            }
        }

        public override void Update(float delta, MyPlane plane)
        {
            base.Update(delta, plane);
            if (IsNetworkMaster())
            {
                if (plane.IsAlive)
                {
                    if (_reverseCD > 0)
                    {
                        _reverseCD -= delta;
                    }

                    _fitDead = 0;

                    // kill is what we want
                    _fitKill = plane.KillCount * 200;

                    // doing damage and folowing a target help to kill
                    _fitDamage = plane.DamageCount * 0.1;
                    _fitFolow += (2.9f - forwardZone.DetectionDistance - forwardUpZone.DetectionDistance - forwardDownZone.DetectionDistance) > 0 ? delta * 5 : 0;

                    // to do damage we need to shoot
                    // disabled -> waste ammo
                    _fitAmmo = 0; // Math.Min(100, plane.GetChildren().OfType<MyGun>().Select(g => g.MaxAmmoCount - g.AmmoCount).Sum());

                    // to folow a target we need to fly
                    _fitAlt = Math.Min(50, Math.Max(MyConst.PxToAltitude(plane.Position.y) * 0.5f, _fitAlt));
                    _fitdistance = Math.Min(100, Math.Max(_fitdistance, Math.Abs(plane.Position.x - _oldX)));

                    // We also want them to survive
                    // disabled -> they can wait to get more point
                    _fitTime = 0; // Math.Min(4, _fitTime + delta * 2);
                                  // disabled -> they can kill themself to prevent been passive
                    _fitPassive = 0; // Math.Max(_fitPassive - (plane.Velocity.LengthSquared() < 20 ? delta : 0), -4);

                    MyNeuralHelper.SetFitness(plane.Identification.ServerId, Fitness);

                    WriteToNeural();
                    NeuralUpdate();
                    if (_skipRead < 0)
                    {
                        ReadFromNeural();
                    }
                    else
                    {
                        _skipRead--;
                    }

                } else
                {
                    // we want to live
                    _fitDead = -100;
                }

                plane.DebugInfo = "" + Fitness;
            }
        }

        private void WriteToNeural()
        {
            int i = 0;
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, MyConst.PxToAltitude(_plane.Position.y) / _plane.MaxAltitude);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.Rotation / (2*Mathf.Pi));
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.EngineThrottle);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.Velocity.x);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.Velocity.y);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.ForwardVector.x);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.ForwardVector.y);
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, Mathf.Clamp(_plane.Stability,0,1));
            MyNeuralHelper.NetworkComputer.SetInput(_network, i++, _plane.Reversed ? 1 : -1);
            for (int y = 0; y < _zones.Length; y++)
            {
                MyNeuralHelper.NetworkComputer.SetInput(_network, i + y, _zones[y].DetectionDistance);
                MyNeuralHelper.NetworkComputer.SetInput(_network, i + y + _zones.Length, _zones[y].DetectionElevation);
            }
        }
        private void NeuralUpdate()
        {
            // auto
        }
        private void ReadFromNeural()
        {
            _throttle = MyNeuralHelper.NetworkComputer.GetOutput(_network, 0) * 2 - 1;
            _gun1 = MyNeuralHelper.NetworkComputer.GetOutput(_network, 1);
            _gun2 = MyNeuralHelper.NetworkComputer.GetOutput(_network, 2);
            _gun3 = MyNeuralHelper.NetworkComputer.GetOutput(_network, 3);
            _gun4 = MyNeuralHelper.NetworkComputer.GetOutput(_network, 4);
            _rotate = MyNeuralHelper.NetworkComputer.GetOutput(_network, 5) * 2 - 1;
            _reverse = MyNeuralHelper.NetworkComputer.GetOutput(_network, 6);
        }

        #region PlayerInput

        public override bool PlaneDecreaseThrustPressed()
        {
            return _throttle < -DoubleActivationValue;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return _throttle > DoubleActivationValue;
        }
        public override bool PlanePrimaryGunPressed()
        {
            return _gun1 > ActivationValue;
        }

        public override bool PlaneSecondaryGunPressed()
        {
            return _gun2 > ActivationValue;
        }

        public override bool PlaneThirdGunPressed()
        {
            return _gun3 > ActivationValue;
        }

        public override bool PlaneFourthGunPressed()
        {
            return _gun4 > ActivationValue;
        }

        public override float PlaneRotatePressed()
        {
            return Mathf.Clamp(_rotate, 0, 1);
        }

        public override float PlaneRotateInvPressed()
        {
            return Mathf.Clamp(-_rotate, 0, 1);
        }

        public override bool PlaneReverseJustPressed()
        {
            if (_reverseCD >= 0 && _reverse > ActivationValue)
            {
                _reverseCD = ReverseCoolDown;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return null;
        }

        #endregion
    }
}
