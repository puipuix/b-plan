﻿using Godot;
using GDExtension;
using System;

namespace BPlan.Game.Common
{
    public class MyNetWorkPlayerInput : MyPlayerInput
    {
        public override EMyPlayerInputType Type => EMyPlayerInputType.Local;

        [Master]
        private bool _reverse = false;
        [Master]
        private bool _decrease = false;
        [Master]
        private bool _increase = false;
        [Master]
        private float _rotate = 0;
        [Master]
        private float _rotateInv = 0;
        [Master]
        private bool _gun1 = false;
        [Master]
        private bool _gun2 = false;
        [Master]
        private bool _gun3 = false;
        [Master]
        private bool _gun4 = false;

        private Vector2? _turret = null;

        [Master]
        private void RSetTurret(Vector2 vector, bool isNull)
        {
            _turret = isNull ? (Vector2?)null : vector;
        }


        public override void Update(float delta, MyPlane plane)
        {
            base.Update(delta, plane);
            var nw = plane.Identification.GetClient()?.NetworkId;
            if (nw.HasValue)
            {
                if (nw.Value == GetTree().GetNetworkUniqueId())
                {
                    if (_plane.Battle.UIFocused)
                    {
                        _reverse = _decrease = _increase = _gun1 = _gun2 = _gun3 = _gun4 = false;
                        _rotate = _rotateInv = 0;
                        _turret = null;
                    }
                    else
                    {
                        _reverse = plane.Battle.TouchScreen.FlushReverseJustPressed() || Input.IsActionJustPressed("plane_reverse");
                        _decrease = _plane.Battle.TouchScreen.DecreaseThrustPressed || Input.IsActionPressed("plane_decrease_thrust");
                        _increase = _plane.Battle.TouchScreen.IncreaseThrustPressed || Input.IsActionPressed("plane_increase_thrust");
                        _rotate = Math.Max(_plane.Battle.TouchScreen.TurnPressed, Input.GetActionStrength("plane_rotate"));
                        _rotateInv = Math.Max(_plane.Battle.TouchScreen.TurnInvPressed, Input.GetActionStrength("plane_rotate_inv"));

                        _gun1 = (_plane.Battle.TouchScreen.TurretPressed && _plane.PrimaryGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_primary_gun");
                        _gun2 = (_plane.Battle.TouchScreen.TurretPressed && _plane.SecondaryGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_secondary_gun");
                        _gun3 = (_plane.Battle.TouchScreen.TurretPressed && _plane.ThirdGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_third_gun");
                        _gun4 = (_plane.Battle.TouchScreen.TurretPressed && _plane.FourthGunType == Components.EmyGunType.Turret)
                            || _plane.Battle.TouchScreen.GunPressed || Input.IsActionPressed("plane_fourth_gun");

                        if (_plane.IsAccessible())
                        {
                            if (_plane.Battle.TouchScreen.Enabled)
                            {
                                _turret = _plane.GlobalPosition + _plane.Battle.TouchScreen.TurretDirection * 1000;
                            }
                            else
                            {
                                _turret = _plane.GetGlobalMousePosition();
                            }
                        }
                        else
                        {
                            _turret = null;
                        }

                    }

                    if (!IsNetworkMaster())
                    {
                        RsetId(1, nameof(_reverse), _reverse);
                        RsetId(1, nameof(_decrease), _decrease);
                        RsetId(1, nameof(_increase), _increase);
                        RsetId(1, nameof(_gun1), _gun1);
                        RsetId(1, nameof(_gun2), _gun2);
                        RsetId(1, nameof(_gun3), _gun3);
                        RsetId(1, nameof(_gun4), _gun4);
                        RsetId(1, nameof(_rotate), _rotate);
                        RsetId(1, nameof(_rotateInv), _rotateInv);
                        RpcId(1, nameof(RSetTurret), _turret ?? Vector2.Zero, _turret is null);
                    }
                }
            }
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return _decrease;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return _increase;
        }

        public override bool PlaneReverseJustPressed()
        {
            return _reverse;
        }

        public override float PlaneRotateInvPressed()
        {
            return _rotate;
        }

        public override float PlaneRotatePressed()
        {
            return _rotateInv;
        }

        public override bool PlanePrimaryGunPressed()
        {
            return _gun1;
        }

        public override bool PlaneSecondaryGunPressed()
        {
            return _gun2;
        }
        public override bool PlaneThirdGunPressed()
        {
            return _gun3;
        }

        public override bool PlaneFourthGunPressed()
        {
            return _gun4;
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return _turret;
        }
    }
}
