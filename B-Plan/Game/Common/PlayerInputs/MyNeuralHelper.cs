﻿using GDExtension;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml.Linq;

namespace BPlan.Game.Common.PlayerInputs
{
    public static class MyNeuralHelper
    {
        public static MyNeuralNetworkComputer NetworkComputer { get; private set; }

        // bot to networkid
        private static Dictionary<int, (int network, int generation, int mutation, string name)>[] _botToData = { new Dictionary<int, (int network, int generation, int mutation, string name)>(), new Dictionary<int, (int network, int generation, int mutation, string name)>() };
        private static Dictionary<int, int> _networkToBot = new Dictionary<int, int>();

        // bot to fitness
        private static Dictionary<int, double> _fitnesss = new Dictionary<int, double>();

        private static List<(int, double)> GetFitness()
        {
            return _botToData[0].Select(kp => (kp.Value.network, GetFitness(kp.Key))).ToList();
        }

        private static double GetFitness(int firstBotId)
        {
            return _fitnesss.GetOrDefault(firstBotId, 0) + _fitnesss.GetOrDefault(firstBotId+1, 0);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="count">At least 3 network will be created.</param>
        /// <param name="startBotIndex"></param>
        public static void CreatePopulation(MyBattle battle, int startBotIndex)
        {
            if (!NetworkComputer.IsAccessible())
            {
                GDEDebug.Print("Creating new neural network computer...");
                GDEDebug.AddTab();
                NetworkComputer = MyNeuralNetworkComputer.Instance(9 + MyNeuralPlayerInput.DetectZoneCount * 2, 7, 21, 2, battle.IACount);
                battle.Menu.SetNNVisual(NetworkComputer);
                GDEDebug.Print("done.");
                GDEDebug.RemoveTab();
                GDEDebug.Print("Creating save folders.");
                GDEDebug.AddTab();
                var d = new Directory();
                var err = d.MakeDirRecursive("user://NeuralNetwork/Backup");
                if (err == Error.Ok)
                {
                    GDEDebug.Print("done.");
                } else
                {
                    GDEDebug.PrintErr("NeuralNetwork/Backup: " + err);
                }
                GDEDebug.RemoveTab();

            }
            GDEDebug.Print("Creating new population...");
            GDEDebug.AddTab();
            int keep, randomized, childs;
            List<(int, double)> neufit;

            // if it's the first population
            if (_botToData[0].IsEmptyC())
            {
                randomized = Math.Max(1, battle.IACount); // we want only randomized networks
                keep = childs = 0;
                neufit = new List<(int, double)>();
            }
            else
            {
                GDEDebug.Print("Auto save");
                GDEDebug.AddTab();
                Save("Backup/backup_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xml");
                GDEDebug.RemoveTab();
               neufit = GetFitness();
                keep = Math.Max(1, Math.Min((int)(battle.IACount * 0.2), neufit.Count)); // 20% same (or less if not enought networks) + 20% mutated
                randomized = Math.Max(1, (int)(battle.IACount * 0.2)); // 20% new
                childs = Math.Max(0, battle.IACount - keep * 2 - randomized); // 40% childs
            }

            GDEDebug.Print("\tFrom size: " + neufit.Count);
            GDEDebug.Print("\tTo size: " + (keep * 2 + randomized + childs) + " (Keep " + keep + ", altered " + keep + ", randomize " + randomized + ", childs " + childs + ")");
            var list = NetworkComputer.ProcessNaturalSelection(neufit, keep, randomized, childs, MyNeuralNetworkComputer.TopRankingSelector)
                .Select<(float[][,] weight, bool mutated, int origin, int origin2), (float[][,] weight, int generation, int mutation, string name)>(d =>
                {
                    if (d.origin == -1)
                    {
                        return (d.weight, 0, 0, "" + GDERandom.RNG.NextChar('A', 'Z'));
                    }
                    else if (d.origin2 == -1)
                    {
                        var n = GetForId(_networkToBot[d.origin]);
                        if (d.mutated)
                        {
                            return (d.weight, n.generation, n.mutation + 1, n.name);
                        }
                        else
                        {
                            return (d.weight, n.generation, n.mutation, n.name);
                        }
                    }
                    else
                    {
                        var n1 = GetForId(_networkToBot[d.origin]);
                        var n2 = GetForId(_networkToBot[d.origin2]);
                        if (n1.generation < n2.generation) // set n1 as the max generation
                        {
                            var tmp = n1;
                            n1 = n2;
                            n2 = tmp;
                        }
                        return (d.weight, n1.generation + 1, 0, "" + GDERandom.RNG.NextChar('A', 'Z') + ' ' + n1.name.Split(' ')[0] + ' ' + n2.name.Split(' ')[0]);
                    }
                }).ToList();

            ClearNetworks();

            // we create the good amount of networks
            if (NetworkComputer.Networks != battle.IACount * 2)
            {
                NetworkComputer.Networks = battle.IACount * 2;
            }

            for (int i = 0; i < battle.IACount; i++)
            {
                int n = i * 2;
                NetworkComputer.SetWeights(n, list[i].weight);
                NetworkComputer.SetWeights(n + 1, NetworkComputer.Copy(NetworkComputer.CreateWeightArray(), list[i].weight)); // duplicate of the network
                _botToData[0][startBotIndex + n] = (n, list[i].generation, list[i].mutation, list[i].name);
                _botToData[1][startBotIndex + n + 1] = (n + 1, list[i].generation, list[i].mutation, list[i].name); // the opposite side will use the duplicate
                _networkToBot[n] = startBotIndex + n;
                _networkToBot[n + 1] = startBotIndex + n + 1;
            }
            GDEDebug.Print("Done.");
            GDEDebug.RemoveTab();
        }

        public static void ClearNetworks()
        {
            _botToData[0].Clear();
            _botToData[1].Clear();
            _networkToBot.Clear();
            GDEDebug.Print("Network data cleared.");
        }

        
        public static void Load(string fileName)
        {
            string path = "user://NeuralNetwork/" + fileName;
            GDEDebug.Print("Loading: " + path);
            GDEDebug.AddTab();
            var f = new File();
            var err = f.Open(path, File.ModeFlags.Read);
            if (err == Error.Ok)
            {
                string txt = f.GetAsText();
                f.Close();
                var xml = XElement.Parse(txt);
                int generation = int.Parse(xml.Attribute("generation").Value, CultureInfo.InvariantCulture);
                int mutation = int.Parse(xml.Attribute("mutation").Value, CultureInfo.InvariantCulture);
                string name = xml.Attribute("name").Value;
                //double fitness = double.Parse(xml.Attribute("fitness").Value, CultureInfo.InvariantCulture);

                var weights = NetworkComputer.CreateWeightArray();
                XElement layers = xml.Element("Layers");
                foreach (var layer in layers.Elements("Layer"))
                {
                    int n = int.Parse(layer.Attribute("n").Value, CultureInfo.InvariantCulture);
                    XElement ws = layer.Element("Weights");
                    foreach (var w in ws.Elements("W"))
                    {
                        int x = int.Parse(w.Attribute("x").Value, CultureInfo.InvariantCulture);
                        int y = int.Parse(w.Attribute("y").Value, CultureInfo.InvariantCulture);
                        float v = float.Parse(w.Attribute("v").Value, CultureInfo.InvariantCulture);
                        weights[n][x, y] = v;
                    }
                }

                ClearNetworks();
                _botToData[0].Add(-1, (0, generation, mutation, name));
                _networkToBot.Add(0, -1);
                NetworkComputer.SetWeights(0, weights);

                GDEDebug.Print("Done.");
            } else
            {
                f.Close();
                GDEDebug.PrintErr(path + ": " + err);
            }
            GDEDebug.RemoveTab();
        }
        

        public static void Save(string fileName)
        {
            string path = "user://NeuralNetwork/" + fileName;
            GDEDebug.Print("Saving: " + path);
            int network = MyNeuralNetworkComputer.TopRankingSelector(GetFitness(), 1).First();
            var botId = _networkToBot[network];
            var data = GetForId(botId);
            var weights = NetworkComputer.GetWeights(network);
            XElement layers = new XElement("Layers");
            for (int i = 0; i < weights.Length; i++)
            {
                XElement ws = new XElement("Weights");
                for (int x = 0; x < weights[i].GetLength(0); x++)
                {
                    for (int y = 0; y < weights[i].GetLength(1); y++)
                    {
                        XElement w = new XElement("W",
                            new XAttribute("x", x.ToString(CultureInfo.InvariantCulture)),
                            new XAttribute("y", y.ToString(CultureInfo.InvariantCulture)),
                            new XAttribute("v", weights[i][x, y].ToString(CultureInfo.InvariantCulture)));
                        ws.Add(w);
                    }
                }

                XElement layer = new XElement("Layer",
                    new XAttribute("n", i.ToString(CultureInfo.InvariantCulture)),
                    ws);
                layers.Add(layer);
            }
            XElement xml = new XElement("NeuralNetwork",
                new XAttribute("generation", data.generation.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("mutation", data.mutation.ToString(CultureInfo.InvariantCulture)),
                new XAttribute("name", data.name),
                new XAttribute("fitness", GetFitness(botId).ToString(CultureInfo.InvariantCulture)),
                layers);

            var f = new File();
            
            var err = f.Open(path, File.ModeFlags.Write);
            if (err == Error.Ok)
            {
                f.StoreString(xml.ToString().Replace("  ", "\t"));
                GDEDebug.Print("\tDone.");
            }
            else
            {
                GDEDebug.PrintErr("\t" + path + ": " + err);
            }
            f.Close();
        }

        public static (int network, int generation, int mutation, string name) GetForId(int botId)
        {
            return _botToData[0].GetOrDefault(botId, () => _botToData[1][botId]);
        }

        public static void SetFitness(int id, double value)
        {
            _fitnesss[id] = value;
        }
    }
}
