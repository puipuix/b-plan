﻿using Godot;
using GDExtension;
using System;
using System.Linq;

namespace BPlan.Game.Common
{
    public class MyIaPlayerInput : MyPlayerInput
    {
        private const float TargetSelectionSpeedUp = 3;

        private float ClimbAngle = 15;
        private float ShotAngle = 10;
        private float ShotRange = 800;
        private float TurretShotRange = 1000;
        private float CruiseAltitude = 1500;
        private float MinAltitude = 300;
        private float TargetError = 100;
        private float TargetErrorMaxDistance = 10;

        public override EMyPlayerInputType Type => EMyPlayerInputType.Ia;

        private MyPlane _target;

        private Vector2 _objective;

        private int _rotate = 0;
        private bool _shot = false;
        private bool _turret = false;
        private bool _rocket = false;
        private bool _bomb = false;

        private float _targetReset = 20f * TargetSelectionSpeedUp;
        private float _targetErrorReset = 1f;
        private bool _reverse = false;
        private Vector2 _targetError = Vector2.Zero;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _target = null;
            }
        }

        private float GetClimbAngle(float altitude)
        {
            if (altitude < MinAltitude)
            {
                return Mathf.Deg2Rad(5 + (ClimbAngle - 5) * Mathf.Pow(altitude / MinAltitude, 0.5f));
            }
            else
            {
                return Mathf.Deg2Rad(ClimbAngle);
            }
        }

        public override void Initialize(MyPlane plane)
        {
            base.Initialize(plane);
            ClimbAngle = GDERandom.RNG.Next(10, 45);
            ShotAngle = GDERandom.RNG.Next(5, 20);
            ShotRange = GDERandom.RNG.Next(1000, 2000);
            TurretShotRange = GDERandom.RNG.Next(1000, 2000);
            int max = -(int)(MyConst.AltitudeToPx(plane.MaxAltitude) * 0.75f);
            int min = Math.Min(max, 500 - (int)(plane.Manoeuvrability * plane.EngineThrust));
            int avg = min + (int)((max - min) * 0.5f);
            CruiseAltitude = GDERandom.RNG.Next(avg, max);
            MinAltitude = GDERandom.RNG.Next(min, avg);
            TargetError = GDERandom.RNG.Next(10, 300);
            TargetErrorMaxDistance = GDERandom.RNG.Next(300, 2000);
        }

        public override void Update(float delta, MyPlane plane)
        {
            base.Update(delta, plane);
            if (_plane.IsAlive)
            {
                _targetReset -= delta;
                _targetErrorReset -= delta;
                _reverse = false;

                // when down vector is negative, reversed should be true
                // so if not we reverse
                _reverse = _plane.DownVector.y < 0 ^ _plane.Reversed;

                if (_targetErrorReset < 0)
                {
                    _targetErrorReset = GDERandom.RNG.NextFloat(0.1f, 1);
                    _targetError = GDERandom.RNG.NextVector2(-1, 1) * TargetError;
                }

                // look for the closest target
                if (_targetReset < 0)
                {
                    _targetReset = GDERandom.RNG.NextFloat(3, 5);
                    _target = _plane.Battle.GetPlanes()
                        .Where(p => !p.Identification.IsFriendly(_plane.Identification) && p.IsAlive) // LF alive enemies
                        .Select(p => (p, p.Position.DistanceSquaredTo(plane.Position))) // calculate distance
                        .FindMin((a, b) => a.Item2.CompareTo(b.Item2)).p; // select closest one
                }

                float climbAngle = GetClimbAngle(-_plane.Position.y);
                // tell if the angle of the plane is low enought to prevent been killed by the ground
                bool flatFlight = Math.Abs(_plane.ForwardVector.y) < Mathf.Sin(climbAngle);
                if ((-_plane.Position.y < MinAltitude && !flatFlight) || !_target.IsAccessible() || !_target.IsAlive)
                {
                    _targetReset -= delta * (TargetSelectionSpeedUp - 1); // speed up the target selection, -1 because we already add delta once

                    // here we calculate a position at the cruise altitude to make the plane clibe at ClimbAngle.
                    Vector2 leftRight = new Vector2(_plane.ForwardVector.x < 0 ? -1 : 1, 0);
                    if (-_plane.Position.y < CruiseAltitude)
                    {
                        float dist = Math.Abs(CruiseAltitude + _plane.Position.y) * Mathf.Tan(Mathf.Pi * 0.5f - climbAngle);
                        _objective = new Vector2(_plane.Position.x, -CruiseAltitude) + leftRight * dist;
                    }
                    else
                    {
                        _objective = _plane.Position + leftRight * 100;
                    }
                    _shot = false;
                    if (_target.IsAccessible())
                    {
                        _turret = (_plane.Position - _target.Position).Length() < TurretShotRange;
                    }
                    else
                    {
                        _turret = false;
                    }
                }
                else
                {
                    // The distance vector and value from target to me
                    Vector2 targToPlane = _plane.Position - _target.Position;
                    float dist = targToPlane.Length();
                    _turret = dist < TurretShotRange;
                    // here _target has a value so we look if he's range here.
                    _shot = dist < ShotRange;

                    _rocket = _shot && _target.Velocity.Length() < 500 && -_target.Position.y < 150;
                    _bomb = _rocket && Math.Abs(targToPlane.x) < 300;

                    // The difference between the bullet velocity and the target velocity.
                    float bulletVelosDiffWithTarget = (_plane.Velocity - _target.Velocity).Length() + 1550;

                    // We take the time to reach the target position
                    float time = dist / bulletVelosDiffWithTarget;
                    if (Math.Abs(_target.AngularVelocity) > 0.1f)
                    {
                        Vector2 ttl = Vector2.Zero;
                        int theoCount = (int)(time / delta);
                        int count = Math.Min(theoCount, 50); // Limit to 50 tick
                        float scale = (float)theoCount / count;
                        // We need to define the scale of the new delta, in case theoCount is greater than 50
                        // (We need to take all the travel time)
                        Vector2 linSpeed = _target.Velocity * delta * scale;
                        float angSpeed = _target.AngularVelocity * delta * scale;
                        while (count > 0)
                        {
                            count--;
                            ttl = (ttl + linSpeed).Rotated(angSpeed);
                        }
                        _objective = _target.Position + ttl - _plane.Velocity * time;
                    }
                    else
                    {
                        _objective = _target.Position + (_target.Velocity - _plane.Velocity) * time;
                    }

                    // Add some random to the aim
                    _objective += _targetError * Math.Min(dist / TargetErrorMaxDistance, 1);

                    // Balistic
                    _objective -= new Vector2(0, 0.5f * MyConst.Gravity * time * time);

                    if (targToPlane.x < -_plane.Battle.MapLimit || _plane.Battle.MapLimit < targToPlane.x)
                    {
                        _objective += new Vector2(2 * _plane.Battle.MapLimit * (_objective.x < 0 ? 1 : -1), 0);
                    }
                }

                Vector2 objectiveDirection = _objective - _plane.Position;

                float angle = Mathf.Rad2Deg(_plane.ForwardVector.AngleTo(objectiveDirection));
                float absAngle = Math.Abs(angle);
                if (-_plane.Position.y < MinAltitude && absAngle > 90)
                {
                    _rotate = _plane.ForwardVector.x > 0 ? 1 : -1;
                    _shot = false;
                    _rocket = false;
                }
                else
                {
                    if (_shot)
                    {
                        Vector2 targetDirection = _target.Position - _plane.Position;
                        // use to check if the plane isn't behind us
                        float verificationAngle = Math.Abs(Mathf.Rad2Deg(_plane.ForwardVector.AngleTo(targetDirection)));
                        _shot = absAngle < ShotAngle && verificationAngle < 90;
                        _rocket = _rocket && _shot;
                    }

                    _rotate = (int)-angle;
                }

                _plane.BotAim.CastTo = objectiveDirection.Rotated(-_plane.Rotation);
            }
            else
            {
                _plane.BotAim.Hide();
                _shot = false;
                _turret = false;
                _rocket = false;
                _bomb = false;
                _rotate = 0;
                _reverse = false;
            }
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return true;
        }

        public override bool PlaneReverseJustPressed()
        {
            return _reverse;
        }

        public override float PlaneRotateInvPressed()
        {
            return _rotate < 0 ? 1 : 0;
        }

        public override float PlaneRotatePressed()
        {
            return _rotate > 0 ? 1 : 0;
        }

        public override bool PlanePrimaryGunPressed()
        {
            switch (_plane.PrimaryGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }
        public override bool PlaneSecondaryGunPressed()
        {
            switch (_plane.SecondaryGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override bool PlaneThirdGunPressed()
        {
            switch (_plane.ThirdGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override bool PlaneFourthGunPressed()
        {
            switch (_plane.FourthGunType)
            {
                case Components.EmyGunType.Gun:
                    return _shot;
                case Components.EmyGunType.Turret:
                    return _turret;
                case Components.EmyGunType.Bomb:
                    return _bomb;
                case Components.EmyGunType.Rocket:
                    return _rocket;
                default:
                    return false;
            }
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return _target.IsAccessible() ? _target.GlobalPosition : (Vector2?)null;
        }
    }
}
