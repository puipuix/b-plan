﻿using BPlan.Game.Common.PlayerInputs;
using Godot;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game.Common
{
    public enum EMyPlayerInputType : byte { None, Local, Ia, Neural }

    public abstract class MyPlayerInput : Node
    {
        public abstract EMyPlayerInputType Type { get; }
        public abstract float PlaneRotatePressed();
        public abstract float PlaneRotateInvPressed();
        public abstract bool PlaneReverseJustPressed();
        public abstract bool PlaneIncreaseThrustPressed();
        public abstract bool PlaneDecreaseThrustPressed();
        public abstract bool PlanePrimaryGunPressed();
        public abstract bool PlaneThirdGunPressed();
        public abstract bool PlaneFourthGunPressed();
        public abstract bool PlaneSecondaryGunPressed();

        public abstract Vector2? PlaneTurretGlobalTarget();

        protected MyPlane _plane;

        public override void _Ready()
        {
            base._Ready();
            Name = "Player Input";
        }

        public virtual void Update(float delta, MyPlane plane) { }
        public virtual void PostUpdate(float delta, MyPlane myPlane) { }

        public static MyPlayerInput NewFromType(EMyPlayerInputType type)
        {
            switch (type)
            {
                case EMyPlayerInputType.Local:
                    return new MyNetWorkPlayerInput();
                case EMyPlayerInputType.Ia:
                    return new MyIaPlayerInput();
                case EMyPlayerInputType.Neural:
                    return MyNeuralPlayerInput.Instance();
                default:
                    return new MyNonePlayerInput();
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                _plane = null;
            }
        }

        public virtual void Initialize(MyPlane plane)
        {
            _plane = plane;
        }

    }
}
