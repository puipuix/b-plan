using GDExtension;
using Godot;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public class MyDetectionZone : Area2D
{
    private static readonly PackedScene _scene = GD.Load<PackedScene>("res://Game/Common/PlayerInputs/MyDetectionZone.tscn");
    public static MyDetectionZone Instance()
    {
        var det = (MyDetectionZone)_scene.Instance();
        return det;
    }

    [Export]
    public float ScanDistance { get; set; } = 2000;

    [GDEReady("Distance", EGDEResearchMode.Path)]
    private Line2D _distance = null;

    [GDEReady("Shape", EGDEResearchMode.Path)]
    private CollisionShape2D _collisionShape = null;

    private HashSet<Node2D> _planes = new HashSet<Node2D>();

    public MyPlane Plane { get; set; }

    public float DetectionDistance { get; private set; } = 1;
    public float DetectionElevation { get; private set; } = 0;

    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
        _collisionShape.Shape = (ConvexPolygonShape2D)_collisionShape.Shape.Duplicate();
        if (IsNetworkMaster())
        {
            this.Connect<Node>("body_entered", this, OnBodyEntered);
            this.Connect<Node>("body_exited", this, OnBodyExited);
        }
    }

    private void OnBodyExited(Node body)
    {
        if (body is MyPlane p)
        {
            _planes.Remove(p);
        }
    }

    private void OnBodyEntered(Node body)
    {
        if (body is MyPlane p && !Plane.Identification.IsFriendly(p.Identification) && p.IsAlive)
        {
            _planes.Add(p);
        }
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (Plane?.IsAlive ?? false)
        {
            (float, float) d;
            if (_planes.IsEmptyT())
            {
                DetectionDistance = 1;
                DetectionElevation = 0;
                d = (0.01f, 0);
            }
            else
            {
                d = _planes
                    .Select(p => (GlobalPosition.DistanceSquaredTo(p.GlobalPosition), GetAngleTo(p.GlobalPosition)))
                    .FindMin((a, b) => a.Item1 > b.Item1 ? 1 : a.Item1 < b.Item1 ? -1 : 0);
                d.Item1 = Mathf.Sqrt(d.Item1);
                DetectionDistance = d.Item1 / ScanDistance; // keep between 0 and 1
                DetectionElevation = d.Item2 / (2 * Mathf.Pi);
            }
            _distance.Points = new Vector2[] { Vector2.Zero, new Vector2(d.Item1, 0).Rotated(d.Item2) };
        }
    }
}
