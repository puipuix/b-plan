using GDExtension;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

//[Tool]
public class MyNeuralNetworkComputer : MarginContainer
{
    public delegate IEnumerable<int> NaturalSelector(IEnumerable<(int network, double fitness)> population, int keep);

    public static readonly NaturalSelector ProbabilisticSelector = (population, keep) =>
    {
        var p = population.Normalized(t => t.fitness, (t, v) => (t.network, v));
        double sum = p.Sum(a => a.fitness);
        Random r = new Random();
        return Enumerable.Range(0, keep).Select(i =>
        {
            double random = r.NextDouble() * sum;
            return p.SkipWhile(a => (random -= a.fitness) >= 0).First().network;
        }).ToList();
    };

    public static readonly NaturalSelector TopRankingSelector = (population, keep) =>
    {
        return population
        .Sorted((a, b) => -(a.fitness > b.fitness ? 1 : a.fitness < b.fitness ? -1 : 0)) // greater first
        .Take(keep)
        .Select(a => a.network)
        .ToList();
    };

    private static Shader _shader = GD.Load<Shader>("res://Content/Shaders/NeuralNetworkLayerShader.shader");
    private static Shader _shader2 = GD.Load<Shader>("res://Content/Shaders/RToRGShader.shader");
    private static PackedScene _scene = GD.Load<PackedScene>("res://Game/Common/PlayerInputs/MyNeuralNetworkComputer.tscn");

    public static MyNeuralNetworkComputer Instance(int inputs, int outputs, int hiddens, int layers, int networks)
    {
        var i = (MyNeuralNetworkComputer)_scene.Instance();
        i.Layers = layers;
        i.Inputs = inputs;
        i.Hiddens = hiddens;
        i.Outputs = outputs;
        i.Networks = networks;
        return i;
    }

    private int _inputs = 1, _outputs = 1, _hiddens = 1;
    private int _layers = 2;
    private int _networks = 1;
    private bool _ready = false;

    [GDEReady("Anchor", EGDEResearchMode.Descendant)]
    private Node _anchor = null;

    [GDEReady("Debug", EGDEResearchMode.Descendant)]
    private VBoxContainer _debug = null;

    private Viewport _root;
    private Sprite _leef = null;
    private Image _netWorkImage, _resultImage;
    private (Image data, ImageTexture texture, ShaderMaterial shader)[] _weights;

    private ImageTexture _leefTexture => (ImageTexture)_leef.Texture;
    private ImageTexture _debugTexture;
    private int _networkWidth => 2 + _layers;
    private int _weightSize => Math.Max(2, Math.Max(Math.Max(_inputs + 1, _outputs), _hiddens + 1));

    public int Inputs { get => _inputs; set { _inputs = Math.Max(1, value); UpdateSize(); } }

    public int Outputs { get => _outputs; set { _outputs = Math.Max(1, value); UpdateSize(); } }

    public int Hiddens { get => _hiddens; set { _hiddens = Math.Max(1, value); UpdateSize(); } }

    public int Networks { get => _networks; set { _networks = Math.Max(1, value); UpdateSize(); } }

    public int Layers { get => _layers; set { _layers = Math.Max(0, value); UpdateSize(); } }

    [Export]
    public float MutationProbability { get; set; } = 1;

    [Export]
    public float MutationFactor { get; set; } = 1;

    [Export]
    public float RandomizeFactor { get; set; } = 1;

    public void SetInput(int network, int input, float value)
    {
        if (float.IsNaN(value) || float.IsInfinity(value))
        {
            throw new ArithmeticException("Value is NaN or infinite: " + value);
        }
        _netWorkImage.Lock();
        // we flip Y because opengl flip Y
        _netWorkImage.SetPixel(network * _networkWidth, _weightSize - input - 1, new Color(Math.Max(0, -value), Math.Max(0, value), 0));
        _netWorkImage.Unlock();
    }

    public float GetOutput(int network, int output)
    {
        float value;
        _resultImage.Lock();
        var pixel = _resultImage.GetPixel(network * _networkWidth + _networkWidth - 1, output);
        value = pixel.g - pixel.r;
        _resultImage.Unlock();
        if (float.IsNaN(value) || float.IsInfinity(value))
        {
            throw new ArithmeticException("Value is NaN or infinite: " + value +
                " (x=" + (network * _networkWidth + _networkWidth - 1) + ", y=" + output + ",img=" + _resultImage.GetSize() + ")");
        }
        return value;
    }

    public float[][,] GetWeights(int network)
    {
        float[][,] ws = new float[_weights.Length][,];
        for (int w = 0; w < _weights.Length; w++)
        {
            ws[w] = new float[_weightSize, _weightSize];
            _weights[w].data.Lock();
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    var pixel = _weights[w].data.GetPixel(network * _weightSize + i, j);
                    ws[w][i, j] = pixel.g - pixel.r;
                }
            }
            _weights[w].data.Unlock();
        }
        return ws;
    }

    public void SetWeights(int network, float[][,] weights)
    {
        for (int w = 0; w < _weights.Length; w++)
        {
            _weights[w].data.Lock();
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    _weights[w].data.SetPixel(network * _weightSize + i, j, new Color(Math.Max(0, -weights[w][i, j]), Math.Max(0, weights[w][i, j]), 0.0f));
                }
            }
            _weights[w].data.Unlock();
            _weights[w].texture.CreateFromImage(_weights[w].data, 0);
            _weights[w].shader.SetShaderParam("Weights", _weights[w].texture);
        }
    }

    private ShaderMaterial CreateShader(int layer, ImageTexture weight)
    {
        var shader = new ShaderMaterial();
        shader.Shader = _shader;
        shader.SetShaderParam("Inputs", _inputs);
        shader.SetShaderParam("Outputs", _outputs);
        shader.SetShaderParam("Hiddens", _hiddens);

        shader.SetShaderParam("TotalNetwork", _networks);

        shader.SetShaderParam("CurrentLayer", layer);
        shader.SetShaderParam("TotalLayer", _networkWidth);

        shader.SetShaderParam("Weights", weight);
        return shader;
    }

    private ShaderMaterial CreateShader2(float scale)
    {
        var shader = new ShaderMaterial();
        shader.Shader = _shader2;
        shader.SetShaderParam("Scale", scale);
        return shader;
    }

    private Viewport CreateViewport()
    {
        var v = new Viewport();
        v.RenderTargetUpdateMode = Viewport.UpdateMode.Always;
        v.GuiDisableInput = true;
        v.TransparentBg = true;
        v.Usage = Viewport.UsageEnum.Usage3d;
        v.Hdr = true;
        return v;
    }

    private Image CreateWeightImage(float size)
    {
        var w = new Image();
        w.Create((int)size * _networks, (int)size, false, Image.Format.Rgf);
        return w;
    }

    private TextureRect CreateTextureRect()
    {
        var t = new TextureRect();
        t.Expand = true;
        t.StretchMode = TextureRect.StretchModeEnum.KeepAspectCentered;
        t.SizeFlagsHorizontal = (int)SizeFlags.ExpandFill;
        t.SizeFlagsVertical = (int)SizeFlags.ExpandFill;
        return t;
    }

    private void UpdateSize()
    {
        if (_ready)
        {
            GDEDebug.Print(_anchor);
            _anchor.RemoveAllChildren(o => o?.QueueFree());
            _debug.RemoveAllChildren(o => o?.QueueFree());

            //min size 2x2
            Vector2 size = new Vector2(Math.Max(2, _networks * _networkWidth), _weightSize);
            _root = CreateViewport();
            _root.Name = "Output";
            _root.Size = size;
            _anchor.AddChild(_root);

            _netWorkImage = new Image();
            _netWorkImage.Create((int)size.x, (int)size.y, false, Image.Format.Rgf);
            TextureRect tri = CreateTextureRect();
            tri.Material = CreateShader2(1);
            tri.Name = "Input";
            tri.FlipV = true;
            _debug.AddChild(tri);

            _weights = new (Image, ImageTexture, ShaderMaterial)[_layers + 1];

            var parent = _root;
            for (int i = 0; i < _layers; i++)
            {
                var vc = new ViewportContainer();
                var v = CreateViewport();
                v.Name = "" + (_layers + 1 - i);

                var w = CreateWeightImage(size.y);
                var t = new ImageTexture();
                t.CreateFromImage(w, 0);
                var s = CreateShader(_layers + 1 - i, t);
                _weights[_layers - i] = (w, t, s);

                parent.AddChild(vc);
                vc.AddChild(v);
                parent = v;

                vc.RectSize = size;
                v.Size = size;

                vc.Material = s;

                TextureRect tr = CreateTextureRect();
                tr.Texture = t;
                tr.Material = CreateShader2(0.1f);
                tr.Name = "" + (_layers + 1 - i);
                _debug.AddChild(tr);
            }
            var we = CreateWeightImage(size.y);
            var txtr = new ImageTexture();
            txtr.CreateFromImage(we, 0);
            var sm = CreateShader(1, txtr);
            _weights[0] = (we, txtr, sm);


            _leef = new Sprite();
            _leef.Name = "Input";
            _leef.Centered = false;
            _leef.Texture = new ImageTexture();
            _leef.Material = sm;
            parent.AddChild(_leef);
            tri.Texture = _leefTexture;

            TextureRect deb = CreateTextureRect();
            _debugTexture = new ImageTexture();
            deb.Texture = _debugTexture;
            deb.Material = CreateShader2(1f);
            deb.Name = "Output";
            _debug.AddChild(deb);
        }
    }

    public override void _Ready()
    {
        base._Ready();
        GDEReadyAttribute.SetUp(this);
        _ready = true;
        UpdateSize();
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (_root != null)
        {
            _leefTexture.CreateFromImage(_netWorkImage, 0);
            _resultImage = _root.GetTexture().GetData();
            _debugTexture.CreateFromImage(_resultImage, 0);
        }
    }

    public float[][,] CreateWeightArray()
    {
        float[][,] ws = new float[_weights.Length][,];
        for (int w = 0; w < _weights.Length; w++)
        {
            ws[w] = new float[_weightSize, _weightSize];
        }
        return ws;
    }

    public float[][,] Copy(float[][,] ws, float[][,] from)
    {
        for (int w = 0; w < _weights.Length; w++)
        {
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    ws[w][i, j] = from[w][i, j];
                }
            }
        }
        return ws;
    }

    public float[][,] AsChild(float[][,] ws, float[][,] parent1, float[][,] parent2, Random r)
    {
        for (int w = 0; w < _weights.Length; w++)
        {
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    if (r.NextBool())
                    {
                        ws[w][i, j] = parent1[w][i, j];
                    }
                    else
                    {
                        ws[w][i, j] = parent2[w][i, j];
                    }
                }
            }
        }
        return ws;
    }

    public float[][,] Mutate(float[][,] ws, Random r)
    {
        for (int w = 0; w < _weights.Length; w++)
        {
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    if (r.NextBool(MutationProbability))
                    {
                        ws[w][i, j] += r.NextFloatExtended() * MutationFactor;
                    }
                }
            }
        }
        return ws;
    }

    public float[][,] Randomize(float[][,] ws, Random r)
    {
        for (int w = 0; w < _weights.Length; w++)
        {
            for (int i = 0; i < _weightSize; i++)
            {
                for (int j = 0; j < _weightSize; j++)
                {
                    ws[w][i, j] += r.NextFloatExtended() * RandomizeFactor;
                }
            }
        }
        return ws;
    }

    public List<(float[][,] weight, bool mutated, int origin, int origin2)> ProcessNaturalSelection(IEnumerable<(int network, double fitness)> fitness, int keep, int randomizedCount, int childs, NaturalSelector selector)
    {
        var survivor = selector(fitness, keep)
            .Select<int, (float[][,] weight, bool mutated, int origin, int origin2)>(i => (GetWeights(i), false, i, -1))
            .ToList();
        Random r = new Random();

        return survivor
                .Concat(survivor.Select(a => (Mutate(Copy(CreateWeightArray(), a.weight), r), true, a.origin, -1)))
                .Concat(Enumerable.Range(0, randomizedCount).Select(i => (Randomize(CreateWeightArray(), r), false, -1, -1)))
                .Concat(Enumerable.Range(0, childs).Select(i =>
                {
                    var a = CreateWeightArray();
                    if (survivor.Count > 1) // if there are more than 1 survivor
                    {
                        int i1 = r.Next(survivor.Count);
                        int i2 = r.Next(survivor.Count);
                        while (i1 == i2) // we take different parents (there are at least 2)
                        {
                            i2 = r.Next(survivor.Count);
                        }
                        return (AsChild(a, survivor[i1].weight, survivor[i2].weight, r), false, survivor[i1].origin, survivor[i2].origin);
                    }
                    else // else we copy the only survivor
                    {
                        return (Copy(a, survivor[0].weight), false, survivor[0].origin, survivor[0].origin);
                    }
                }))
                .ToList();
    }
}
