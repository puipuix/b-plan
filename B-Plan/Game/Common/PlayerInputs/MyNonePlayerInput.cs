﻿using Godot;

namespace BPlan.Game.Common
{
    public class MyNonePlayerInput : MyPlayerInput
    {
        public override EMyPlayerInputType Type => EMyPlayerInputType.None;

        public override bool PlaneThirdGunPressed()
        {
            return false;
        }

        public override bool PlaneDecreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneIncreaseThrustPressed()
        {
            return false;
        }

        public override bool PlaneReverseJustPressed()
        {
            return false;
        }

        public override bool PlaneFourthGunPressed()
        {
            return false;
        }

        public override float PlaneRotateInvPressed()
        {
            return 0;
        }

        public override float PlaneRotatePressed()
        {
            return 0;
        }

        public override bool PlanePrimaryGunPressed()
        {
            return false;
        }

        public override bool PlaneSecondaryGunPressed()
        {
            return false;
        }

        public override Vector2? PlaneTurretGlobalTarget()
        {
            return null;
        }
    }
}
