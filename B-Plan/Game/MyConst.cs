﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BPlan.Game
{
    public static class MyConst
    {
        public const int PxPerM = 10;
        public const float Gravity = 9.807f * PxPerM;
        public static float PxToAltitude(float yPx) => -yPx / PxPerM;
        public static float AltitudeToPx(float altitude) => -altitude * PxPerM;

        public static float PxToM(float px) => px / PxPerM;
        public static float MToPx(float m) => m * PxPerM;

        public static float PxsToKmh(float velosPx) => (velosPx / PxPerM) * 3.6f; 
        public static float KmhToPxs(float velosKmh) => (velosKmh / 3.6f) * PxPerM; 
    }
}
