shader_type canvas_item;

// COLOR
uniform vec4 Color : hint_color;

// SHAPE
uniform float Fadeout;
uniform float Power;
uniform float MinAlpha;
uniform float Intensity;
uniform float Shadow;
uniform float ShadowDepth;

void fragment() {
	float depth = UV.y * float(textureSize(TEXTURE, 0).y);
	float depthRation = min(1.0, depth / ShadowDepth);
	COLOR.rgb = Color.rbg * (1.0 - depthRation * Shadow);
	
	vec2 coor = UV.xy - vec2(0.5);
	float len = length(coor);
	if (len > 0.5){
		COLOR.a = 0.0;
	} else {
		float pixel = texture(TEXTURE, UV).r;
		float shape = pow(1.0-pixel, -Power);
		float factor =  pow(1.0 - (length(coor) / 0.5), Fadeout);
		if (shape * factor > MinAlpha){
			COLOR.a = min(1.0, factor * pixel * Intensity);
		} else {
			COLOR.a = 0.0;
		}
	}
	
}