shader_type canvas_item;

uniform sampler2D Weights : hint_albedo;
uniform int Inputs;
uniform int Outputs;
uniform int Hiddens;

uniform int TotalNetwork;

uniform int CurrentLayer;
uniform int TotalLayer;

float sigmoid(float x){
	return 1.0/(1.0+exp(-x));
}

vec4 process(int from_neurons, int to_neurons, sampler2D network, vec2 uv, vec2 fragcoor, vec2 pixel_size) {
	if (int(fragcoor.y) < to_neurons) { // if the target neurons exist
		ivec2 networkSize = textureSize(network, 0);
		ivec2 weightSize = textureSize(Weights, 0);
		
		int currentNetwork = int(float(TotalNetwork) * uv.x); // we use the uv to know how far wa are in the networks
		
		float sum = 0.0;
		for (int i = 0; i < from_neurons; i++) { // for each neurons from previous layer
			float wx = float(currentNetwork * weightSize.y + i); // we shift to find the current network data and we add the neurin index
			float uvwx = wx / float(weightSize.x); // we divide the pixel coor by the size
			vec2 wrg = texture(Weights, vec2(uvwx,uv.y)).rg;
			float w = wrg.g - wrg.r;
			
			float uvay = float(i)/float(networkSize.y); // we divide the pixel coor by the size
			float uvax = uv.x - pixel_size.x; // we sub the uv by one pixel
			vec2 arg = texture(network, vec2(uvax,uvay)).rg;
			float a = arg.g - arg.r;
			
			sum += a*w;
		}
		
		float wx = float(currentNetwork * weightSize.y + from_neurons); // we shift to find the current network data and we add the bias index
		float uvwx = wx / float(weightSize.x); // we divide the pixel coor by the size
		vec2 wrg = texture(Weights, vec2(uvwx,uv.y)).rg;
		float bias = wrg.g - wrg.r;
		sum += bias;
		float val = sigmoid(sum);
		return vec4(max(0.0, -val), max(0.0, val), 0.0, 1.0);
	} else {
		return vec4(0.0,0.0,1.0,1.0);
	}
}

void fragment() {
	vec2 pixel = FRAGCOORD.xy; // current neuron
	if (int(pixel.x) % TotalLayer == CurrentLayer){
		if (CurrentLayer == 1){ // input to...
			if (CurrentLayer == TotalLayer-1){ // output
				COLOR = process(Inputs, Outputs, TEXTURE, UV, FRAGCOORD.xy, TEXTURE_PIXEL_SIZE);
			} else { // hidden
				COLOR = process(Inputs, Hiddens, TEXTURE, UV, FRAGCOORD.xy, TEXTURE_PIXEL_SIZE);
			}
		} else { // hidden to ...
			if (CurrentLayer == TotalLayer-1){ // output
				COLOR = process(Hiddens, Outputs, TEXTURE, UV, FRAGCOORD.xy, TEXTURE_PIXEL_SIZE);
			} else { // hidden
				COLOR = process(Hiddens, Hiddens, TEXTURE, UV, FRAGCOORD.xy, TEXTURE_PIXEL_SIZE);
			}
		}
	} else {
		COLOR = texture(TEXTURE, UV);
	}
}