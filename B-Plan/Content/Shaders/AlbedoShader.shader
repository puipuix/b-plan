shader_type canvas_item;

uniform vec4 Albedo : hint_color;
uniform sampler2D Texture : hint_albedo;

void fragment() {
	vec4 textColor = texture(Texture, UV);
	COLOR.rgba = Albedo.rgba * textColor.rgba;
}