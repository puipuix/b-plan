shader_type canvas_item;

uniform float Scale = 1.0;

void fragment() {
	COLOR.rg = Scale*texture(TEXTURE, UV).rg;
	//float r = .r;
	//COLOR.r = clamp(-r, 0.0, 1.0);
	//COLOR.g = clamp(r, 0.0, 1.0);
	COLOR.b = 0.0;
	COLOR.a = 1.0;
}